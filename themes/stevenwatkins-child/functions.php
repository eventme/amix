<?php
/**
 * Child-Theme functions and definitions
 */
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style( 'parent-style_colors', get_stylesheet_directory_uri() . '/css/__colors.css' );
    wp_enqueue_style( 'parent-style_inline', get_stylesheet_directory_uri() . '/css/__inline.css' );
    wp_enqueue_style( 'parent-style_style', get_stylesheet_directory_uri() . '/css/__styles.css' );
    wp_enqueue_style( 'parent-style_style', get_stylesheet_directory_uri() . '/css/responsive.css' );
}

add_action( 'init', 'register_taxonomies' );
function register_taxonomies() {
    register_taxonomy( 'amix_cat',
        array( 'product' ),
        array(
            'hierarchical'          => true,
            'label'                 => __( 'Amix Categories', 'woocommerce' ),
            'labels' => array(
                'name'              => __( 'Amix Categories', 'woocommerce' ),
                'singular_name'     => __( 'Amix Category', 'woocommerce' ),
                'menu_name'         => _x( 'Amix Categories', 'Admin menu name', 'woocommerce' ),
                'search_items'      => __( 'Search Amix Categories', 'woocommerce' ),
                'all_items'         => __( 'All Amix Categories', 'woocommerce' ),
                'parent_item'       => __( 'Parent Amix Category', 'woocommerce' ),
                'parent_item_colon' => __( 'Parent Amix Category:', 'woocommerce' ),
                'edit_item'         => __( 'Edit Amix Category', 'woocommerce' ),
                'update_item'       => __( 'Update Amix Category', 'woocommerce' ),
                'add_new_item'      => __( 'Add New Amix Category', 'woocommerce' ),
                'new_item_name'     => __( 'New Amix Category Name', 'woocommerce' ),
                'not_found'         => __( 'No Amix Category found', 'woocommerce' ),
            ),
            'show_ui'               => true,
            'query_var'             => true,
            'capabilities'          => array(
                'manage_terms' => 'manage_product_terms',
                'edit_terms'   => 'edit_product_terms',
                'delete_terms' => 'delete_product_terms',
                'assign_terms' => 'assign_product_terms',
            ),
            'rewrite'               => array(
                'slug'         => _x( 'amix-category', 'slug', 'woocommerce' ),
                'with_front'   => false,
                'hierarchical' => true,
            ),
        )
    );
    register_taxonomy_for_object_type( 'amix_cat', 'product' );
}

include_once('widgets\amix_category.php');

?>