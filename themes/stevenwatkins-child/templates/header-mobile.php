<?php
/**
 * The template to show mobile menu
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */
?>
<div class="menu_mobile_overlay"></div>
<div class="menu_mobile scheme_dark">
	<div class="menu_mobile_inner">
		<a class="menu_mobile_close icon-cancel"></a><?php

		// Logo
		get_template_part( 'templates/header-logo' );

		// Main menu
		stevenwatkins_show_layout(apply_filters('stevenwatkins_filter_menu_mobile_layout', str_replace(
			array('id="menu_main', 'id="menu-', 'class="menu_main'),
			array('id="menu_mobile', 'id="menu_mobile-', 'class="menu_mobile'),
			get_query_var('stevenwatkins_menu_main')
		)));

		// Search field
		//		?>
		<div class="search_mobile">
			<div class="search_form_wrap">
				<div class="curt_amix_mobile"><a href="<?php echo wc_get_cart_url(); ?>">Корзина</a></div>
				<div class="login_amix_mobile">
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('Мой аккайнт','woothemes'); ?></a>
					<?php }
					else { ?>
						<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Войти / Регистрация','woothemes'); ?></a>
					<?php } ?></div>
			</div>
		</div>
		<?php

		// Social icons
		stevenwatkins_show_layout(stevenwatkins_get_socials_links(), '<div class="socials_mobile">', '</div>');
		?>
	</div>
</div>
