<?php
/**
 * The template for displaying 'Header menu'
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_header_image = get_query_var('stevenwatkins_header_image');

$stevenwatkins_stevenwatkins_menu_header = stevenwatkins_get_nav_menu('menu_header');

// Store menu layout for the mobile menu
set_query_var('stevenwatkins_menu_header', $stevenwatkins_stevenwatkins_menu_header);

if (!empty($stevenwatkins_stevenwatkins_menu_header)) {
	?>
	<div class="top_panel_navi_header 
				<?php if ($stevenwatkins_header_image!='') echo ' with_bg_image'; ?>
				scheme_<?php echo esc_attr(stevenwatkins_is_inherit(stevenwatkins_get_theme_option('header_scheme')) 
													? stevenwatkins_get_theme_option('color_scheme') 
													: stevenwatkins_get_theme_option('header_scheme')
											); ?>">
		<div class="menu_header_wrap clearfix menu_hover_<?php echo esc_attr(stevenwatkins_get_theme_option('menu_hover')); ?>">
			<div class="content_wrap">
				<?php stevenwatkins_show_layout($stevenwatkins_stevenwatkins_menu_header); ?>
			</div>
		</div>
	</div><!-- /.top_panel_navi_top -->
	<?php
}
?>