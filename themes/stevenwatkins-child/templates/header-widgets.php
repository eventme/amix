<?php
/**
 * The template for displaying Header widgets area
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Header sidebar
$stevenwatkins_header_name = stevenwatkins_get_theme_option('header_widgets');
$stevenwatkins_header_present = !stevenwatkins_is_off($stevenwatkins_header_name) && is_active_sidebar($stevenwatkins_header_name);
if ($stevenwatkins_header_present) { 
	stevenwatkins_storage_set('current_sidebar', 'header');
	$stevenwatkins_header_wide = stevenwatkins_get_theme_option('header_wide');
	ob_start();
	do_action( 'stevenwatkins_action_before_sidebar' );
	if ( !dynamic_sidebar($stevenwatkins_header_name) ) {
		// Put here html if user no set widgets in sidebar
	}
	do_action( 'stevenwatkins_action_after_sidebar' );
	$stevenwatkins_widgets_output = ob_get_contents();
	ob_end_clean();
	$stevenwatkins_widgets_output = preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $stevenwatkins_widgets_output);
	$stevenwatkins_need_columns = strpos($stevenwatkins_widgets_output, 'columns_wrap')===false;
	if ($stevenwatkins_need_columns) {
		$stevenwatkins_columns = max(0, (int) stevenwatkins_get_theme_option('header_columns'));
		if ($stevenwatkins_columns == 0) $stevenwatkins_columns = min(6, max(1, substr_count($stevenwatkins_widgets_output, '<aside ')));
		if ($stevenwatkins_columns > 1)
			$stevenwatkins_widgets_output = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($stevenwatkins_columns).' widget ', $stevenwatkins_widgets_output);
		else
			$stevenwatkins_need_columns = false;
	}
	?>
	<div class="header_widgets_wrap widget_area<?php echo !empty($stevenwatkins_header_wide) ? ' header_fullwidth' : ' header_boxed'; ?>">
		<div class="header_widgets_wrap_inner widget_area_inner">
			<?php 
			if (!$stevenwatkins_header_wide) { 
				?><div class="content_wrap"><?php
			}
			if ($stevenwatkins_need_columns) {
				?><div class="columns_wrap"><?php
			}
			stevenwatkins_show_layout($stevenwatkins_widgets_output);
			if ($stevenwatkins_need_columns) {
				?></div>	<!-- /.columns_wrap --><?php
			}
			if (!$stevenwatkins_header_wide) {
				?></div>	<!-- /.content_wrap --><?php
			}
			?>
		</div>	<!-- /.header_widgets_wrap_inner -->
	</div>	<!-- /.header_widgets_wrap -->
<?php
}
?>