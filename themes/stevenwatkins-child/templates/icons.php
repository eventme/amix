<?php
/**
 * The template to displaying popup with Theme Icons
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_icons = stevenwatkins_get_list_icons();
if (is_array($stevenwatkins_icons)) {
	?>
	<div class="stevenwatkins_list_icons">
		<?php
		foreach($stevenwatkins_icons as $icon) {
			?><span class="<?php echo esc_attr($icon); ?>" title="<?php echo esc_attr($icon); ?>"></span><?php
		}
		?>
	</div>
	<?php
}
?>