<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0.06
 */

$stevenwatkins_header_css = $stevenwatkins_header_image = '';
$stevenwatkins_header_video = wp_is_mobile() ? '' : stevenwatkins_get_theme_option('header_video');
if (true || empty($stevenwatkins_header_video)) {
	$stevenwatkins_header_image = get_header_image();
	if (stevenwatkins_is_on(stevenwatkins_get_theme_option('header_image_override')) && apply_filters('stevenwatkins_filter_allow_override_header_image', true)) {
		if (is_category()) {
			if (($stevenwatkins_cat_img = stevenwatkins_get_category_image()) != '')
				$stevenwatkins_header_image = $stevenwatkins_cat_img;
		} else if (is_singular() || stevenwatkins_storage_isset('blog_archive')) {
			if (has_post_thumbnail()) {
				$stevenwatkins_header_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				if (is_array($stevenwatkins_header_image)) $stevenwatkins_header_image = $stevenwatkins_header_image[0];
			} else
				$stevenwatkins_header_image = '';
		}
	}
}

// Store header image for navi
set_query_var('stevenwatkins_header_image', $stevenwatkins_header_image || $stevenwatkins_header_video);

// Get post with custom header
$stevenwatkins_header_id = str_replace('header-custom-', '', stevenwatkins_get_theme_option("header_style"));
$stevenwatkins_header_post = get_post($stevenwatkins_header_id);

if (!empty($stevenwatkins_header_post->post_content)) {
	?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr($stevenwatkins_header_id);
						echo !empty($stevenwatkins_header_image) || !empty($stevenwatkins_header_video) ? ' with_bg_image' : ' without_bg_image';
						if ($stevenwatkins_header_video!='') echo ' with_bg_video';
						if ($stevenwatkins_header_image!='') echo ' '.esc_attr(stevenwatkins_add_inline_style('background-image: url('.esc_url($stevenwatkins_header_image).');'));
						if (is_single() && has_post_thumbnail()) echo ' with_featured_image';
						if (stevenwatkins_is_on(stevenwatkins_get_theme_option('header_fullheight'))) echo ' header_fullheight trx-stretch-height';
						?> scheme_<?php echo esc_attr(stevenwatkins_is_inherit(stevenwatkins_get_theme_option('header_scheme')) 
														? stevenwatkins_get_theme_option('color_scheme') 
														: stevenwatkins_get_theme_option('header_scheme'));
						?>"><?php
		
		stevenwatkins_show_layout(do_shortcode($stevenwatkins_header_post->post_content));
		
	?></header><?php
}
?>