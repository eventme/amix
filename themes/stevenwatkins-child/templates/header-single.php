<?php
/**
 * The template for displaying Featured image in the single post
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

if ( get_query_var('stevenwatkins_header_image')=='' && is_singular() && has_post_thumbnail() && in_array(get_post_type(), array('post', 'page')) )  {
	set_query_var('stevenwatkins_featured_showed', true);
	$stevenwatkins_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
	if (!empty($stevenwatkins_src[0])) {
		?><div class="post_featured post_featured_fullwide <?php echo esc_attr(stevenwatkins_add_inline_style('background-image:url('.esc_url($stevenwatkins_src[0]).');')); ?>"></div><?php
	}
}
?>