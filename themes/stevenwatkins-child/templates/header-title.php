<?php
/**
 * The template for displaying Page title and Breadcrumbs
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Page (category, tag, archive, author) title

if ( stevenwatkins_need_page_title() ) {
	set_query_var('stevenwatkins_title_showed', true);
	$stevenwatkins_top_icon = stevenwatkins_get_category_icon();
	?>
	<div class="top_panel_title_wrap">
		<div class="content_wrap">
			<div class="top_panel_title">
				<div class="page_title">
					<?php
					// Post meta on the single post
					if ( is_single() )  {
					}
					
					// Blog/Post title
					$stevenwatkins_blog_title = stevenwatkins_get_blog_title();
					$stevenwatkins_blog_title_text = $stevenwatkins_blog_title_class = $stevenwatkins_blog_title_link = $stevenwatkins_blog_title_link_text = '';
					if (is_array($stevenwatkins_blog_title)) {
						$stevenwatkins_blog_title_text = $stevenwatkins_blog_title['text'];
						$stevenwatkins_blog_title_class = !empty($stevenwatkins_blog_title['class']) ? ' '.$stevenwatkins_blog_title['class'] : '';
						$stevenwatkins_blog_title_link = !empty($stevenwatkins_blog_title['link']) ? $stevenwatkins_blog_title['link'] : '';
						$stevenwatkins_blog_title_link_text = !empty($stevenwatkins_blog_title['link_text']) ? $stevenwatkins_blog_title['link_text'] : '';
					} else
						$stevenwatkins_blog_title_text = $stevenwatkins_blog_title;
					?>
					<h1 class="page_caption<?php echo esc_attr($stevenwatkins_blog_title_class); ?>"><?php
						if (!empty($stevenwatkins_top_icon)) {
							?><img src="<?php echo esc_url($stevenwatkins_top_icon); ?>" alt=""><?php
						}
						echo wp_kses_data($stevenwatkins_blog_title_text);
					?></h1>
					<?php
					if (!empty($stevenwatkins_blog_title_link) && !empty($stevenwatkins_blog_title_link_text)) {
						?><a href="<?php echo esc_url($stevenwatkins_blog_title_link); ?>" class="theme_button theme_button_small page_title_link"><?php echo esc_html($stevenwatkins_blog_title_link_text); ?></a><?php
					}
					
					// Category/Tag description
					if ( is_category() || is_tag() || is_tax() ) 
						the_archive_description( '<div class="page_description">', '</div>' );
					?>
				</div>
				<?php
				// Breadcrumbs
				if (stevenwatkins_is_on(stevenwatkins_get_theme_option('show_breadcrumbs'))) {
					stevenwatkins_show_layout(stevenwatkins_get_breadcrumbs(), '<div class="breadcrumbs">', '</div>');
				}
				?>
			</div>
		</div>
	</div>
	<?php
}
?>