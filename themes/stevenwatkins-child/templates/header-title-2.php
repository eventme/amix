<?php
/**
 * The template to display image and page description
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_image = stevenwatkins_get_theme_option('header_title_image');
$stevenwatkins_text = stevenwatkins_get_theme_option('header_title_text');
if (!empty($stevenwatkins_image) || !empty($stevenwatkins_text)) {
	?>
	<div class="top_panel_title_2_wrap">
		<div class="content_wrap">
			<div class="top_panel_title_2">
				<?php
				if (!empty($stevenwatkins_image)) {
					$stevenwatkins_attr = stevenwatkins_getimagesize($stevenwatkins_image);
					echo '<div class="top_panel_title_2_image"><img src="'.esc_url($stevenwatkins_image).'" alt=""'.(!empty($stevenwatkins_attr[3]) ? sprintf(' %s', $stevenwatkins_attr[3]) : '').'></div>';
				}
				stevenwatkins_show_layout($stevenwatkins_text, '<div class="top_panel_title_2_text">', '</div>');
				?>
			</div>
		</div>
	</div>
	<?php
}
?>