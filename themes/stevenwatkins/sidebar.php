<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_sidebar_position = stevenwatkins_get_theme_option('sidebar_position');
if (stevenwatkins_sidebar_present()) {
	$stevenwatkins_sidebar_name = stevenwatkins_get_theme_option('sidebar_widgets');
	stevenwatkins_storage_set('current_sidebar', 'sidebar');
	?>
	<div class="sidebar <?php echo esc_attr($stevenwatkins_sidebar_position); ?> widget_area<?php if (!stevenwatkins_is_inherit(stevenwatkins_get_theme_option('sidebar_scheme'))) echo ' scheme_'.esc_attr(stevenwatkins_get_theme_option('sidebar_scheme')); ?>" role="complementary">
		<div class="sidebar_inner">
			<?php
			ob_start();
			do_action( 'stevenwatkins_action_before_sidebar' );
			if ( !dynamic_sidebar($stevenwatkins_sidebar_name) ) {
				// Put here html if user no set widgets in sidebar
			}
			do_action( 'stevenwatkins_action_after_sidebar' );
			$stevenwatkins_out = ob_get_contents();
			ob_end_clean();
			stevenwatkins_show_layout(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $stevenwatkins_out));
			?>
		</div><!-- /.sidebar_inner -->
	</div><!-- /.sidebar -->
	<?php
}
?>