<?php
/**
 * The Classic template for displaying content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_blog_style = explode('_', stevenwatkins_get_theme_option('blog_style'));
$stevenwatkins_columns = empty($stevenwatkins_blog_style[1]) ? 2 : max(2, $stevenwatkins_blog_style[1]);
$stevenwatkins_expanded = !stevenwatkins_sidebar_present() && stevenwatkins_is_on(stevenwatkins_get_theme_option('expand_content'));
$stevenwatkins_post_format = get_post_format();
$stevenwatkins_post_format = empty($stevenwatkins_post_format) ? 'standard' : str_replace('post-format-', '', $stevenwatkins_post_format);
$stevenwatkins_animation = stevenwatkins_get_theme_option('blog_animation');

?><div class="column-1_<?php echo esc_attr($stevenwatkins_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_classic post_layout_classic_'.esc_attr($stevenwatkins_columns).' post_format_'.esc_attr($stevenwatkins_post_format) ); ?>
	<?php echo (!stevenwatkins_is_off($stevenwatkins_animation) ? ' data-animation="'.esc_attr(stevenwatkins_get_animation_classes($stevenwatkins_animation)).'"' : ''); ?>
	>

	<?php

	// Featured image
	stevenwatkins_show_post_featured( array( 'thumb_size' => stevenwatkins_get_thumb_size(
													strpos(stevenwatkins_get_theme_option('body_style'), 'full')!==false 
														? ( $stevenwatkins_columns > 2 ? 'big' : 'huge' )
														: (	$stevenwatkins_columns > 2
															? ($stevenwatkins_expanded ? 'med' : 'small')
															: ($stevenwatkins_expanded ? 'big' : 'med')
															)
														)
										) );

	?>
	<div class="post_header entry-header">
		<?php 
		do_action('stevenwatkins_action_before_post_title'); 

		// Post title
		the_title( sprintf( '<h4 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );

		do_action('stevenwatkins_action_before_post_meta'); 

		// Post meta
		stevenwatkins_show_post_meta(array(
			'categories' => true,
			'date' => true,
			'edit' => $stevenwatkins_columns < 3,
			'seo' => false,
			'share' => false,
			'counters' => 'comments',
			)
		);
		?>
	</div><!-- .entry-header -->
	<?php
	?>

	<div class="post_content entry-content">
		<div class="post_content_inner">
			<?php
			$stevenwatkins_show_learn_more = false; 
			if (has_excerpt()) {
				the_excerpt();
			} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
				the_content( '' );
			} else if (in_array($stevenwatkins_post_format, array('link', 'aside', 'status', 'quote'))) {
				the_content();
			} else if (substr(get_the_content(), 0, 1)!='[') {
				the_excerpt();
			}
			?>
		</div>
		<?php

		// More button
		if ( $stevenwatkins_show_learn_more ) {
			?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'stevenwatkins'); ?></a></p><?php
		}
		?>
	</div><!-- .entry-content -->

</article></div>