<?php
/* Revolution Slider support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('stevenwatkins_revslider_theme_setup9')) {
	add_action( 'after_setup_theme', 'stevenwatkins_revslider_theme_setup9', 9 );
	function stevenwatkins_revslider_theme_setup9() {
		if (is_admin()) {
			add_filter( 'stevenwatkins_filter_tgmpa_required_plugins',	'stevenwatkins_revslider_tgmpa_required_plugins' );
		}
	}
}

// Check if RevSlider installed and activated
if ( !function_exists( 'stevenwatkins_exists_revslider' ) ) {
	function stevenwatkins_exists_revslider() {
		return function_exists('rev_slider_shortcode');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'stevenwatkins_revslider_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('stevenwatkins_filter_tgmpa_required_plugins',	'stevenwatkins_revslider_tgmpa_required_plugins');
	function stevenwatkins_revslider_tgmpa_required_plugins($list=array()) {
		if (in_array('revslider', stevenwatkins_storage_get('required_plugins'))) {
			$path = stevenwatkins_get_file_dir('plugins/revslider/revslider.zip');
			if (file_exists($path)) {
				$list[] = array(
						'name' 		=> esc_html__('Revolution Slider', 'stevenwatkins'),
						'slug' 		=> 'revslider',
						'source'	=> $path,
						'required' 	=> false
				);
			}
		}
		return $list;
	}
}
?>