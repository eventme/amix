<?php
/* Calculate Fields Form support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('stevenwatkins_calculated_fields_form_theme_setup9')) {
	add_action( 'after_setup_theme', 'stevenwatkins_calculated_fields_form_theme_setup9', 9 );
	function stevenwatkins_calculated_fields_form_theme_setup9() {
		if (stevenwatkins_exists_calculated_fields_form()) {
			add_action( 'wp_enqueue_scripts', 							'stevenwatkins_calculated_fields_form_frontend_scripts', 1100 );
			add_filter( 'stevenwatkins_filter_merge_styles',					'stevenwatkins_calculated_fields_form_merge_styles' );
			add_filter( 'stevenwatkins_filter_get_css',						'stevenwatkins_calculated_fields_form_get_css', 10, 3 );
		}
		if (is_admin()) {
			add_filter( 'stevenwatkins_filter_tgmpa_required_plugins',		'stevenwatkins_calculated_fields_form_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'stevenwatkins_exists_calculated_fields_form' ) ) {
	function stevenwatkins_exists_calculated_fields_form() {
		return class_exists('CP_SESSION');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'stevenwatkins_calculated_fields_form_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('stevenwatkins_filter_tgmpa_required_plugins',	'stevenwatkins_calculated_fields_form_tgmpa_required_plugins');
	function stevenwatkins_calculated_fields_form_tgmpa_required_plugins($list=array()) {
		if (in_array('calculated-fields-form', stevenwatkins_storage_get('required_plugins'))) {
			$list[] = array(
					'name' 		=> esc_html__('Calculated Fields Form', 'stevenwatkins'),
					'slug' 		=> 'calculated-fields-form',
					'required' 	=> false
			);
		}
		return $list;
	}
}
	
// Enqueue plugin's custom styles
if ( !function_exists( 'stevenwatkins_calculated_fields_form_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'stevenwatkins_calculated_fields_form_frontend_scripts', 1100 );
	function stevenwatkins_calculated_fields_form_frontend_scripts() {
		// Remove jquery_ui from frontend
		if (stevenwatkins_get_theme_setting('disable_jquery_ui')) {
			global $wp_styles;
			$wp_styles->done[] = 'cpcff_jquery_ui';
		}
		if (stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('plugins/calculated-fields-form/calculated-fields-form.css')))
			stevenwatkins_enqueue_style( 'stevenwatkins-calculated-fields-form',  stevenwatkins_get_file_url('plugins/calculated-fields-form/calculated-fields-form.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'stevenwatkins_calculated_fields_form_merge_styles' ) ) {
	//Handler of the add_filter('stevenwatkins_filter_merge_styles', 'stevenwatkins_calculated_fields_form_merge_styles');
	function stevenwatkins_calculated_fields_form_merge_styles($list) {
		$list[] = 'plugins/calculated-fields-form/calculated-fields-form.css';
		return $list;
	}
}



// Add plugin's specific styles into color scheme
//------------------------------------------------------------------------

// Add styles into CSS
if ( !function_exists( 'stevenwatkins_calculated_fields_form_get_css' ) ) {
	//Handler of the add_filter( 'stevenwatkins_filter_get_css', 'stevenwatkins_calculated_fields_form_get_css', 10, 3 );
	function stevenwatkins_calculated_fields_form_get_css($css, $colors, $fonts) {
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS

CSS;
		}

		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

#fbuilder .dfield select,
#fbuilder .dfield input {
	color: {$colors['text_light']};
 	border-color: {$colors['inverse_dark']};
 	background-color:{$colors['alter_bg_color']};
}

CSS;
		}
		
		return $css;
	}
}
?>