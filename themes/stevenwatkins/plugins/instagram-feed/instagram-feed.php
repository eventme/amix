<?php
/* Instagram Feed support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('stevenwatkins_instagram_feed_theme_setup9')) {
	add_action( 'after_setup_theme', 'stevenwatkins_instagram_feed_theme_setup9', 9 );
	function stevenwatkins_instagram_feed_theme_setup9() {
		if (is_admin()) {
			add_filter( 'stevenwatkins_filter_tgmpa_required_plugins',		'stevenwatkins_instagram_feed_tgmpa_required_plugins' );
		}
	}
}

// Check if Instagram Feed installed and activated
if ( !function_exists( 'stevenwatkins_exists_instagram_feed' ) ) {
	function stevenwatkins_exists_instagram_feed() {
		return defined('SBIVER');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'stevenwatkins_instagram_feed_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('stevenwatkins_filter_tgmpa_required_plugins',	'stevenwatkins_instagram_feed_tgmpa_required_plugins');
	function stevenwatkins_instagram_feed_tgmpa_required_plugins($list=array()) {
		if (in_array('instagram-feed', stevenwatkins_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> esc_html__('Instagram Feed', 'stevenwatkins'),
					'slug' 		=> 'instagram-feed',
					'required' 	=> false
				);
		return $list;
	}
}
?>