<?php
/* Mail Chimp support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('stevenwatkins_mailchimp_theme_setup9')) {
	add_action( 'after_setup_theme', 'stevenwatkins_mailchimp_theme_setup9', 9 );
	function stevenwatkins_mailchimp_theme_setup9() {
		if (stevenwatkins_exists_mailchimp()) {
			add_action( 'wp_enqueue_scripts',							'stevenwatkins_mailchimp_frontend_scripts', 1100 );
			add_filter( 'stevenwatkins_filter_merge_styles',					'stevenwatkins_mailchimp_merge_styles');
			add_filter( 'stevenwatkins_filter_get_css',						'stevenwatkins_mailchimp_get_css', 10, 3);
		}
		if (is_admin()) {
			add_filter( 'stevenwatkins_filter_tgmpa_required_plugins',		'stevenwatkins_mailchimp_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'stevenwatkins_exists_mailchimp' ) ) {
	function stevenwatkins_exists_mailchimp() {
		return function_exists('__mc4wp_load_plugin') || defined('MC4WP_VERSION');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'stevenwatkins_mailchimp_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('stevenwatkins_filter_tgmpa_required_plugins',	'stevenwatkins_mailchimp_tgmpa_required_plugins');
	function stevenwatkins_mailchimp_tgmpa_required_plugins($list=array()) {
		if (in_array('mailchimp-for-wp', stevenwatkins_storage_get('required_plugins')))
			$list[] = array(
				'name' 		=> esc_html__('MailChimp for WP', 'stevenwatkins'),
				'slug' 		=> 'mailchimp-for-wp',
				'required' 	=> false
			);
		return $list;
	}
}



// Custom styles and scripts
//------------------------------------------------------------------------

// Enqueue custom styles
if ( !function_exists( 'stevenwatkins_mailchimp_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'stevenwatkins_mailchimp_frontend_scripts', 1100 );
	function stevenwatkins_mailchimp_frontend_scripts() {
		if (stevenwatkins_exists_mailchimp()) {
			if (stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('plugins/mailchimp-for-wp/mailchimp-for-wp.css')))
				stevenwatkins_enqueue_style( 'stevenwatkins-mailchimp-for-wp',  stevenwatkins_get_file_url('plugins/mailchimp-for-wp/mailchimp-for-wp.css'), array(), null );
		}
	}
}
	
// Merge custom styles
if ( !function_exists( 'stevenwatkins_mailchimp_merge_styles' ) ) {
	//Handler of the add_filter( 'stevenwatkins_filter_merge_styles', 'stevenwatkins_mailchimp_merge_styles');
	function stevenwatkins_mailchimp_merge_styles($list) {
		$list[] = 'plugins/mailchimp-for-wp/mailchimp-for-wp.css';
		return $list;
	}
}

// Add css styles into global CSS stylesheet
if (!function_exists('stevenwatkins_mailchimp_get_css')) {
	//Handler of the add_filter('stevenwatkins_filter_get_css', 'stevenwatkins_mailchimp_get_css', 10, 3);
	function stevenwatkins_mailchimp_get_css($css, $colors, $fonts) {
		
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS

.mc4wp-form-fields .mc4wp-field.sc_item_button button {
	{$fonts['h5_font-family']}
}

CSS;
		}
		
		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

.mc4wp-form input[type="email"] {
	border-color: {$colors['alter_bd_color']};
	color: {$colors['text']};
}
.mc4wp-form button {
	color: {$colors['inverse_dark']};
}
.mc4wp-form button:hover {
	color: {$colors['inverse_dark']};
}

CSS;
		}

		return $css;
	}
}
?>