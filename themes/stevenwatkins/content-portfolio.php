<?php
/**
 * The Portfolio template for displaying content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_blog_style = explode('_', stevenwatkins_get_theme_option('blog_style'));
$stevenwatkins_columns = empty($stevenwatkins_blog_style[1]) ? 2 : max(2, $stevenwatkins_blog_style[1]);
$stevenwatkins_post_format = get_post_format();
$stevenwatkins_post_format = empty($stevenwatkins_post_format) ? 'standard' : str_replace('post-format-', '', $stevenwatkins_post_format);
$stevenwatkins_animation = stevenwatkins_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_portfolio_'.esc_attr($stevenwatkins_columns).' post_format_'.esc_attr($stevenwatkins_post_format) ); ?>
	<?php echo (!stevenwatkins_is_off($stevenwatkins_animation) ? ' data-animation="'.esc_attr(stevenwatkins_get_animation_classes($stevenwatkins_animation)).'"' : ''); ?>
	>

	<?php
	$stevenwatkins_image_hover = stevenwatkins_get_theme_option('image_hover');
	// Featured image
	stevenwatkins_show_post_featured(array(
		'thumb_size' => stevenwatkins_get_thumb_size(strpos(stevenwatkins_get_theme_option('body_style'), 'full')!==false || $stevenwatkins_columns < 3 ? 'masonry-big' : 'masonry'),
		'show_no_image' => true,
		'class' => $stevenwatkins_image_hover == 'dots' ? 'hover_with_info' : '',
		'post_info' => $stevenwatkins_image_hover == 'dots' ? '<div class="post_info">'.esc_html(get_the_title()).'</div>' : ''
	));
	?>
</article>