<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the Visual Composer to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

// Get template page's content
$stevenwatkins_content = '';
$stevenwatkins_blog_archive_mask = '%%CONTENT%%';
$stevenwatkins_blog_archive_subst = sprintf('<div class="blog_archive">%s</div>', $stevenwatkins_blog_archive_mask);
if ( have_posts() ) {
	the_post(); 
	if (($stevenwatkins_content = apply_filters('the_content', get_the_content())) != '') {
		if (($stevenwatkins_pos = strpos($stevenwatkins_content, $stevenwatkins_blog_archive_mask)) !== false) {
			$stevenwatkins_content = preg_replace('/(\<p\>\s*)?'.$stevenwatkins_blog_archive_mask.'(\s*\<\/p\>)/i', $stevenwatkins_blog_archive_subst, $stevenwatkins_content);
		} else
			$stevenwatkins_content .= $stevenwatkins_blog_archive_subst;
		$stevenwatkins_content = explode($stevenwatkins_blog_archive_mask, $stevenwatkins_content);
	}
}

// Make new query
$stevenwatkins_args = array(
	'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish'
);
$stevenwatkins_args = stevenwatkins_query_add_posts_and_cats($stevenwatkins_args, '', stevenwatkins_get_theme_option('post_type'), stevenwatkins_get_theme_option('parent_cat'));
$stevenwatkins_page_number = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
if ($stevenwatkins_page_number > 1) {
	$stevenwatkins_args['paged'] = $stevenwatkins_page_number;
	$stevenwatkins_args['ignore_sticky_posts'] = true;
}
$stevenwatkins_ppp = stevenwatkins_get_theme_option('posts_per_page');
if ((int) $stevenwatkins_ppp != 0)
	$stevenwatkins_args['posts_per_page'] = (int) $stevenwatkins_ppp;

query_posts( $stevenwatkins_args );

// Set query vars in the new query!
if (is_array($stevenwatkins_content) && count($stevenwatkins_content) == 2) {
	set_query_var('blog_archive_start', $stevenwatkins_content[0]);
	set_query_var('blog_archive_end', $stevenwatkins_content[1]);
}

get_template_part('index');
?>