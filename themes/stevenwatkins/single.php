<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

get_header();

while ( have_posts() ) { the_post();

	get_template_part( 'content', get_post_format() );

	if (stevenwatkins_get_theme_option('post_navigation_show') > 0) {
		// Previous/next post navigation.

		?><div class="nav-links-single"><?php
			the_post_navigation( array(
				'next_text' => '<span class="nav-arrow"></span>'
					. '<span class="screen-reader-text">' . esc_html__( 'Next post:', 'stevenwatkins' ) . '</span> '
					. '<h6 class="post-title">%title</h6>'
					. '<span class="post_date">%date</span>',
				'prev_text' => '<span class="nav-arrow"></span>'
					. '<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'stevenwatkins' ) . '</span> '
					. '<h6 class="post-title">%title</h6>'
					. '<span class="post_date">%date</span>'
			) );
		?></div>
	<?php
		}
	?>
		<?php
	
	if (stevenwatkins_get_theme_option('related_posts_show') > 0) {
	// Related posts.
	// You can specify style 1|2 in the second parameter
		stevenwatkins_show_related_posts(array(
											'orderby' => 'post_date',	// put here 'rand' if you want to show posts in random order
											'order' => 'DESC',
											'numberposts' => 2
											), 2);
	}

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
}

get_footer();
?>