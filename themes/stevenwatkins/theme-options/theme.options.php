<?php
/**
 * Default Theme Options and Internal Theme Settings
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// -----------------------------------------------------------------
// -- ONLY FOR PROGRAMMERS, NOT FOR CUSTOMER
// -- Internal theme settings
// -----------------------------------------------------------------
stevenwatkins_storage_set('settings', array(
	
	'custom_sidebars'			=> 8,							// How many custom sidebars will be registered (in addition to theme preset sidebars): 0 - 10

	'ajax_views_counter'		=> true,						// Use AJAX for increment posts counter (if cache plugins used) 
																// or increment posts counter then loading page (without cache plugin)
	'disable_jquery_ui'			=> false,						// Prevent loading custom jQuery UI libraries in the third-party plugins

	'max_load_fonts'			=> 3,							// Max fonts number to load from Google fonts or from uploaded fonts

	'breadcrumbs_max_level' 	=> 3,							// Max number of the nested categories in the breadcrumbs (0 - unlimited)

	'use_mediaelements'			=> true,						// Load script "Media Elements" to play video and audio

	'max_excerpt_length'		=> 60,							// Max words number for the excerpt in the blog style 'Excerpt'.
																// For style 'Classic' - get half from this value
	'message_maxlength'			=> 1000							// Max length of the message from contact form
	
));



// -----------------------------------------------------------------
// -- Theme fonts (Google and/or custom fonts)
// -----------------------------------------------------------------

// Fonts to load when theme start
// It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
// Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
// For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'
stevenwatkins_storage_set('load_fonts', array(
	array(
		'name'   => 'metropolis',
		'family' => 'sans-serif'
		),
    array(
        'name'   => 'bebas',
        'family' => 'sans-serif'
    ),
    array(
        'name'   => 'bebasb',
        'family' => 'sans-serif'
    ),
	array(
		'name'	 => 'rajdhani',
		'family' => 'sans-serif'
		)

));

// Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
stevenwatkins_storage_set('load_fonts_subset', 'latin,latin-ext');

// Settings of the main tags
stevenwatkins_storage_set('theme_fonts', array(
	'p' => array(
		'title'				=> esc_html__('Main text', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the main text of the site', 'stevenwatkins'),
		'font-family'		=> '"metropolis", sans-serif',
		'font-size' 		=> '1rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.625em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'none',
		'letter-spacing'	=> '0',
		'margin-top'		=> '0em',
		'margin-bottom'		=> '1.55em'
		),
	'h1' => array(
		'title'				=> esc_html__('Heading 1', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '4.87rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.01em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '0.82em',
		'margin-bottom'		=> '0.45em'
		),
	'h2' => array(
		'title'				=> esc_html__('Heading 2', 'stevenwatkins'),
		'font-family'		=> '"bebasb", sans-serif',
		'font-size' 		=> '3rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.1em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '1.3em',
		'margin-bottom'		=> '0.76em'
		),
	'h3' => array(
		'title'				=> esc_html__('Heading 3', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '2.25rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '1.25em',
		'margin-bottom'		=> '0.55em'
		),
	'h4' => array(
		'title'				=> esc_html__('Heading 4', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '1.875rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.1em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '1.75em',
		'margin-bottom'		=> '0.7em'
		),
	'h5' => array(
		'title'				=> esc_html__('Heading 5', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '1.5rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.1em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '1.7em',
		'margin-bottom'		=> '0.9em'
		),
	'h6' => array(
		'title'				=> esc_html__('Heading 6', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '1rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.15em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '',
		'margin-top'		=> '2.3em',
		'margin-bottom'		=> '1.75em'
		),
	'logo' => array(
		'title'				=> esc_html__('Logo text', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the text case of the logo', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '1.6em',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.25em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '1px'
		),
	'button' => array(
		'title'				=> esc_html__('Buttons', 'stevenwatkins'),
		'font-family'		=> '"bebas", sans-serif',
		'font-size' 		=> '12px',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.5em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '2px'
		),
	'input' => array(
		'title'				=> esc_html__('Input fields', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the input fields, dropdowns and textareas', 'stevenwatkins'),
		'font-family'		=> '"Open Sans", sans-serif',
		'font-size' 		=> '0.75rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'none',
		'letter-spacing'	=> '0.04em'
		),
	'info' => array(
		'title'				=> esc_html__('Post meta', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the post meta: date, counters, share, etc.', 'stevenwatkins'),
		'font-family'		=> '"Open Sans", sans-serif',
		'font-size' 		=> '0.9rem',
		'font-weight'		=> '400',
		'font-style'		=> 'normal',
		'line-height'		=> '1.5em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'none',
		'letter-spacing'	=> '',
		'margin-top'		=> '0.4em',
		'margin-bottom'		=> ''
		),
	'menu' => array(
		'title'				=> esc_html__('Main menu', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the main menu items', 'stevenwatkins'),
		'font-family'		=> '"Open Sans", sans-serif',
		'font-size' 		=> '1.188rem',
		'font-weight'		=> '600',
		'font-style'		=> 'normal',
		'line-height'		=> '1.5em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '0'
		),
	'submenu' => array(
		'title'				=> esc_html__('Dropdown menu', 'stevenwatkins'),
		'description'		=> esc_html__('Font settings of the dropdown menu items', 'stevenwatkins'),
		'font-family'		=> '"Open Sans", sans-serif',
		'font-size' 		=> '1.188rem',
		'font-weight'		=> '600',
		'font-style'		=> 'normal',
		'line-height'		=> '1.5em',
		'text-decoration'	=> 'none',
		'text-transform'	=> 'uppercase',
		'letter-spacing'	=> '0'
		)
));


// -----------------------------------------------------------------
// -- Theme colors for customizer
// -- Attention! Inner scheme must be last in the array below
// -----------------------------------------------------------------
stevenwatkins_storage_set('schemes', array(

	// Color scheme: 'default'
	'default' => array(
		'title'	 => esc_html__('Default', 'stevenwatkins'),
		'colors' => array(
			
			// Whole block border and background
			'bg_color'				=> '#ffffff', //
			'bd_color'				=> '#ffffff', //

			// Text and links colors
			'text'					=> '#676b70', //
			'text_light'			=> '#989da5', //
			'text_dark'				=> '#17191f', //
			'text_link'				=> '#17191f', //
			'text_hover'			=> '#ec3449', //

			// Alternative blocks (submenu, buttons, tabs, etc.)
			'alter_bg_color'		=> '#f3f4f7', //
			'alter_bg_hover'		=> '#e9e9eb', //
			'alter_bd_color'		=> '#f3f4f8', //
			'alter_bd_hover'		=> '#ced5d9',
			'alter_text'			=> '#3f4346',
			'alter_light'			=> '#a0b2be',
			'alter_dark'			=> '#13162b',
			'alter_link'			=> '#3fb9be',
			'alter_hover'			=> '#13162b',

			// Input fields (form's fields and textarea)
			'input_bg_color'		=> '#f3f4f7', //
			'input_bg_hover'		=> '#ebf4fa',
			'input_bd_color'		=> '#f3f4f7', //
			'input_bd_hover'		=> '#989da5', //
			'input_text'			=> '#989da5', //
			'input_light'			=> '#8b9ba6',
			'input_dark'			=> '#76838c',
			
			// Inverse blocks (text and links on accented bg)
			'inverse_text'			=> '#f0f0f0',
			'inverse_light'			=> '#f7f7f7',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff',
			'inverse_hover'			=> '#13162b',

			// Additional accented colors (if used in the current theme)
			// For example:
			'accent'				=> '#3a52d8', //
			'accent2'				=> '#ec3449', //
			'accent3'				=> '#20232c', //
		
		)
	),

	// Color scheme: 'dark'
	'dark' => array(
		'title'  => esc_html__('Dark', 'stevenwatkins'),
		'colors' => array(
			
			// Whole block border and background
			'bg_color'				=> '#17191f', //
			'bd_color'				=> '#333947', //

			// Text and links colors
			'text'					=> '#989da5', //
			'text_light'			=> '#FFFFFF', //
			'text_dark'				=> '#ec3449', //
			'text_link'				=> '#FFFFFF', //
			'text_hover'			=> '#ec3449', //

			// Alternative blocks (submenu, buttons, tabs, etc.)
			'alter_bg_color'		=> '#ffffff', //
			'alter_bg_hover'		=> '#20232c',
			'alter_bd_color'		=> '#484c55', //
			'alter_bd_hover'		=> '#1f254d',
			'alter_text'			=> '#20232c', //
			'alter_light'			=> '#b8c3cc',
			'alter_dark'			=> '#ffffff',
			'alter_link'			=> '#3fb9be',
			'alter_hover'			=> '#ffffff',

			// Input fields (form's fields and textarea)
			'input_bg_color'		=> '#0e1123',
			'input_bg_hover'		=> '#181e3d',
			'input_bd_color'		=> '#181e3d',
			'input_bd_hover'		=> '#181e3d',
			'input_text'			=> '#969fa6',
			'input_light'			=> '#b8c3cc',
			'input_dark'			=> '#ffffff',
			
			// Inverse blocks (text and links on accented bg)
			'inverse_text'			=> '#f0f0f0',
			'inverse_light'			=> '#f7f7f7',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff',
			'inverse_hover'			=> '#13162b',
		
			// Additional accented colors (if used in the current theme)
			// For example:
			'accent'				=> '#3a52d8', //
			'accent2'				=> '#ec3449', //
			'accent3'				=> '#333947', //

		)
	)

));



// -----------------------------------------------------------------
// -- Theme options for customizer
// -----------------------------------------------------------------
if (!function_exists('stevenwatkins_options_create')) {

	function stevenwatkins_options_create() {

		stevenwatkins_storage_set('options', array(
		
			// Section 'Title & Tagline' - add theme options in the standard WP section
			'title_tagline' => array(
				"title" => esc_html__('Title, Tagline & Site icon', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify site title and tagline (if need) and upload the site icon', 'stevenwatkins') ),
				"type" => "section"
				),
		
		
			// Section 'Header' - add theme options in the standard WP section
			'header_image' => array(
				"title" => esc_html__('Header', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload logo images, select header type and widgets set for the header', 'stevenwatkins') ),
				"type" => "section"
				),
			'header_image_override' => array(
				"title" => esc_html__('Header image override', 'stevenwatkins'),
				"desc" => wp_kses_data( __("Allow override the header image with the page's/post's/product's/etc. featured image", 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_video' => array(
				"title" => esc_html__('Header video', 'stevenwatkins'),
				"desc" => wp_kses_data( __("Select video to use it as background for the header", 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"hidden" => true,
				"std" => '',
				"type" => "video"
				),
			'header_fullheight' => array(
				"title" => esc_html__('Fullheight Header', 'stevenwatkins'),
				"desc" => wp_kses_data( __("Enlarge header area to fill whole screen. Used only if header have a background image", 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_style' => array(
				"title" => esc_html__('Header style', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select style to display the site header', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 'header-default',
				"options" => apply_filters('stevenwatkins_filter_list_header_styles', array(
					'header-1' => esc_html__('Default Header',	'stevenwatkins')
				)),
				"type" => "select"
				),
			'header_position' => array(
				"title" => esc_html__('Header position', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select position to display the site header', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 'default',
				"options" => array(
					'default' => esc_html__('Default','stevenwatkins'),
					'over' => esc_html__('Over',	'stevenwatkins'),
					'under' => esc_html__('Under',	'stevenwatkins')
				),
				"type" => "select"
				),
			'header_scheme' => array(
				"title" => esc_html__('Header Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to decorate header area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 'inherit',
				"options" => stevenwatkins_get_list_schemes(true),
				"refresh" => false,
				"type" => "select"
				),
			'menu_style' => array(
				"title" => esc_html__('Menu position', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select position of the main menu', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 'top',
				"options" => array(
					'top'	=> esc_html__('Top',	'stevenwatkins'),
					// 'left'	=> esc_html__('Left',	'stevenwatkins'),
					'right'	=> esc_html__('Right',	'stevenwatkins')
				),
				// "dependency" => array(
				// 	'header_style' => array('header-2')
				// ),
				"type" => "switch"
				),
			'menu_scheme' => array(
				"title" => esc_html__('Menu Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to decorate main menu area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 'inherit',
				"options" => stevenwatkins_get_list_schemes(true),
				"refresh" => false,
				"type" => "select"
				),
			'menu_stretch' => array(
				"title" => esc_html__('Stretch sidemenu', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Stretch sidemenu to window height (if menu items number >= 5)', 'stevenwatkins') ),
				"std" => 1,
				"type" => "checkbox"
				),
			'search_style' => array(
				"title" => esc_html__('Search in the header', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select style of the search field in the header', 'stevenwatkins') ),
				"std" => 'expand',
				"options" => array(
					'expand' => esc_html__('Expand', 'stevenwatkins')
				),
				"hidden" => true,
				"type" => "switch"
				),
			'header_widgets' => array(
				"title" => esc_html__('Header widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on each page', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on this page', 'stevenwatkins') ),
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'header_columns' => array(
				"title" => esc_html__('Header columns', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select number columns to show widgets in the Header. If 0 - autodetect by the widgets count', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"dependency" => array(
					'header_widgets' => array('^hide')
				),
				"std" => 0,
				"options" => stevenwatkins_get_list_range(0,6),
				"type" => "select"
				),
			'header_wide' => array(
				"title" => esc_html__('Header fullwide', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Do you want to stretch the header widgets area to the entire window width?', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 1,
				"type" => "checkbox"
				),
			'show_page_title' => array(
				"title" => esc_html__('Show Page Title', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Do you want to show page title area (page/post/category title and breadcrumbs)?', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'stevenwatkins')
				),
				"std" => 1,
				"type" => "checkbox"
				),
			'show_breadcrumbs' => array(
				"title" => esc_html__('Show breadcrumbs', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Do you want to show breadcrumbs in the page title area?', 'stevenwatkins') ),
				"std" => 1,
				"type" => "checkbox"
				),
			'logo' => array(
				"title" => esc_html__('Logo', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload site logo', 'stevenwatkins') ),
				"std" => '',
				"type" => "image"
				),
			'logo_retina' => array(
				"title" => esc_html__('Logo for Retina', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload site logo used on Retina displays (if empty - use default logo from the field above)', 'stevenwatkins') ),
				"std" => '',
				"type" => "image"
				),
			'mobile_layout_width' => array(
				"title" => esc_html__('Mobile layout from', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Window width to show mobile layout of the header', 'stevenwatkins') ),
				"std" => 959,
				"type" => "text"
				),
			
		
		
			// Section 'Content'
			'content' => array(
				"title" => esc_html__('Content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Options for the content area', 'stevenwatkins') ),
				"type" => "section",
				),
			'body_style' => array(
				"title" => esc_html__('Body style', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select width of the body content', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"refresh" => false,
				"std" => 'wide',
				"options" => array(
					'boxed'		=> esc_html__('Boxed',		'stevenwatkins'),
					'wide'		=> esc_html__('Wide',		'stevenwatkins')
				),
				"type" => "select"
				),
			'color_scheme' => array(
				"title" => esc_html__('Site Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to decorate whole site. Attention! Case "Inherit" can be used only for custom pages, not for root site content in the Appearance - Customize', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"std" => 'default',
				"options" => stevenwatkins_get_list_schemes(true),
				"refresh" => false,
				"type" => "select"
				),
			'expand_content' => array(
				"title" => esc_html__('Expand content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'remove_margins' => array(
				"title" => esc_html__('Remove margins', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Remove margins above and below the content area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"refresh" => false,
				"std" => 0,
				"type" => "checkbox"
				),
			'seo_snippets' => array(
				"title" => esc_html__('SEO snippets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Add structured data markup to the single posts and pages', 'stevenwatkins') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'no_image' => array(
				"title" => esc_html__('No image placeholder', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload image, used as placeholder for the posts without featured image', 'stevenwatkins') ),
				"std" => '',
				"type" => "image"
				),
			'sidebar_widgets' => array(
				"title" => esc_html__('Sidebar widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select default widgets to show in the sidebar', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'sidebar_scheme' => array(
				"title" => esc_html__('Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to decorate sidebar', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'default',
				"options" => stevenwatkins_get_list_schemes(true),
				"refresh" => false,
				"type" => "select"
				),
			'sidebar_position' => array(
				"title" => esc_html__('Sidebar position', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select position to show sidebar', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"refresh" => false,
				"std" => 'right',
				"options" => stevenwatkins_get_list_sidebars_positions(),
				"type" => "select"
				),
			'widgets_above_page' => array(
				"title" => esc_html__('Widgets above the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_above_content' => array(
				"title" => esc_html__('Widgets above the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_content' => array(
				"title" => esc_html__('Widgets below the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_page' => array(
				"title" => esc_html__('Widgets below the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'stevenwatkins')
				),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
		
		
		
			// Section 'Footer'
			'footer' => array(
				"title" => esc_html__('Footer', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select set of widgets and columns number for the site footer', 'stevenwatkins') ),
				"type" => "section"
				),
			'footer_scheme' => array(
				"title" => esc_html__('Footer Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Footer', 'stevenwatkins')
				),
				"std" => 'dark',
				"options" => stevenwatkins_get_list_schemes(true),
				"refresh" => false,
				"type" => "select"
				),
			'footer_widgets' => array(
				"title" => esc_html__('Footer widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Footer', 'stevenwatkins')
				),
				"std" => 'footer_widgets',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'footer_columns' => array(
				"title" => esc_html__('Footer columns', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses',
					'section' => esc_html__('Footer', 'stevenwatkins')
				),
				"dependency" => array(
					'footer_widgets' => array('^hide')
				),
				"std" => 0,
				"options" => stevenwatkins_get_list_range(0,6),
				"type" => "select"
				),
			'logo_in_footer' => array(
				"title" => esc_html__('Show logo', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show logo in the footer', 'stevenwatkins') ),
				'refresh' => false,
				"std" => 0,
				"hidden" => true,
				"type" => "checkbox"
				),
			'logo_footer' => array(
				"title" => esc_html__('Logo for footer', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload site logo to display it in the footer', 'stevenwatkins') ),
				"dependency" => array(
					'logo_in_footer' => array('1')
				),
				"std" => '',
				"hidden" => true,
				"type" => "image"
				),
			'logo_footer_retina' => array(
				"title" => esc_html__('Logo for footer (Retina)', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select or upload logo for the footer area used on Retina displays (if empty - use default logo from the field above)', 'stevenwatkins') ),
				"dependency" => array(
					'logo_in_footer' => array('1')
				),
				"std" => '',
				"hidden" => true,
				"type" => "image"
				),
			'socials_in_footer' => array(
				"title" => esc_html__('Show social icons', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show social icons in the footer (under logo or footer widgets)', 'stevenwatkins') ),
				"std" => 0,
				"hidden" => true,
				"type" => "checkbox"
				),
			'copyright' => array(
				"title" => esc_html__('Copyright', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Copyright text in the footer', 'stevenwatkins') ),
				"std" => esc_html__('ThemeREX &copy; {Y}. All rights reserved. Terms of use and Privacy Policy', 'stevenwatkins'),
				"hidden" => true,
				"refresh" => false,
				"type" => "textarea"
				),
		
		
		
			// Section 'Homepage' - settings for home page
			'homepage' => array(
				"title" => esc_html__('Homepage', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select blog style and widgets to display on the homepage', 'stevenwatkins') ),
				"type" => "section"
				),
			'expand_content_home' => array(
				"title" => esc_html__('Expand content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden on the Homepage', 'stevenwatkins') ),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'blog_style_home' => array(
				"title" => esc_html__('Blog style', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select posts style for the homepage', 'stevenwatkins') ),
				"std" => 'excerpt',
				"options" => stevenwatkins_get_list_blog_styles(),
				"type" => "select"
				),
			'first_post_large_home' => array(
				"title" => esc_html__('First post large', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Make first post large (with Excerpt layout) on the Classic layout of the Homepage', 'stevenwatkins') ),
				"dependency" => array(
					'blog_style_home' => array('classic')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_widgets_home' => array(
				"title" => esc_html__('Header widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on the homepage', 'stevenwatkins') ),
				"std" => 'header_widgets',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'sidebar_widgets_home' => array(
				"title" => esc_html__('Sidebar widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select sidebar to show on the homepage', 'stevenwatkins') ),
				"std" => 'sidebar_widgets',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'sidebar_position_home' => array(
				"title" => esc_html__('Sidebar position', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select position to show sidebar on the homepage', 'stevenwatkins') ),
				"refresh" => false,
				"std" => 'right',
				"options" => stevenwatkins_get_list_sidebars_positions(),
				"type" => "select"
				),
			'widgets_above_page_home' => array(
				"title" => esc_html__('Widgets above the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_above_content_home' => array(
				"title" => esc_html__('Widgets above the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_content_home' => array(
				"title" => esc_html__('Widgets below the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_page_home' => array(
				"title" => esc_html__('Widgets below the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			
		
		
			// Section 'Blog archive'
			'blog' => array(
				"title" => esc_html__('Blog archive', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Options for the blog archive', 'stevenwatkins') ),
				"type" => "section",
				),
			'expand_content_blog' => array(
				"title" => esc_html__('Expand content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden on the blog archive', 'stevenwatkins') ),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'blog_style' => array(
				"title" => esc_html__('Blog style', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select posts style for the blog archive', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php')
				),
				"std" => 'excerpt',
				"options" => stevenwatkins_get_list_blog_styles(),
				"type" => "select"
				),
			'blog_columns' => array(
				"title" => esc_html__('Blog columns', 'stevenwatkins'),
				"desc" => wp_kses_data( __('How many columns should be used in the blog archive (from 2 to 4)?', 'stevenwatkins') ),
				"std" => 2,
				"options" => stevenwatkins_get_list_range(2,4),
				"type" => "hidden"
				),
			'post_type' => array(
				"title" => esc_html__('Post type', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select post type to show in the blog archive', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php')
				),
				"linked" => 'parent_cat',
				"refresh" => false,
				"hidden" => true,
				"std" => 'post',
				"options" => stevenwatkins_get_list_posts_types(),
				"type" => "select"
				),
			'parent_cat' => array(
				"title" => esc_html__('Category to show', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select category to show in the blog archive', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php')
				),
				"refresh" => false,
				"hidden" => true,
				"std" => '0',
				"options" => stevenwatkins_array_merge(array(0 => esc_html__('- Select category -', 'stevenwatkins')), stevenwatkins_get_list_categories()),
				"type" => "select"
				),
			'posts_per_page' => array(
				"title" => esc_html__('Posts per page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('How many posts will be displayed on this page', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php')
				),
				"hidden" => true,
				"std" => '10',
				"type" => "text"
				),
			"blog_pagination" => array( 
				"title" => esc_html__('Pagination style', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show Older/Newest posts or Page numbers below the posts list', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"std" => "pages",
				"options" => array(
					'pages'	=> esc_html__("Page numbers", 'stevenwatkins'),
					'links'	=> esc_html__("Older/Newest", 'stevenwatkins')
				),
				"type" => "select"
				),
			'show_filters' => array(
				"title" => esc_html__('Show filters', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show categories as tabs to filter posts', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php'),
					'blog_style' => array('portfolio', 'gallery')
				),
				"hidden" => true,
				"std" => 0,
				"type" => "checkbox"
				),
			'first_post_large' => array(
				"title" => esc_html__('First post large', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Make first post large (with Excerpt layout) on the Classic layout of blog archive', 'stevenwatkins') ),
				"dependency" => array(
					'blog_style' => array('classic')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			"blog_content" => array( 
				"title" => esc_html__('Posts content', 'stevenwatkins'),
				"desc" => wp_kses_data( __("Show full post's content in the blog or only post's excerpt", 'stevenwatkins') ),
				"std" => "excerpt",
				"options" => array(
					'excerpt'	=> esc_html__('Excerpt',	'stevenwatkins'),
					'fullpost'	=> esc_html__('Full post',	'stevenwatkins')
				),
				"type" => "select"
				),
			'time_diff_before' => array(
				"title" => esc_html__('Time difference', 'stevenwatkins'),
				"desc" => wp_kses_data( __("How many days show time difference instead post's date", 'stevenwatkins') ),
				"std" => 5,
				"type" => "text"
				),
			'post_navigation_show' => array(
				"title" => esc_html__('Show post navigation', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show post navigation in the single post?', 'stevenwatkins') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'related_posts_show' => array(
				"title" => esc_html__('Show Related posts', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Show related posts in the single post?', 'stevenwatkins') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'related_posts' => array(
				"title" => esc_html__('Related posts', 'stevenwatkins'),
				"desc" => wp_kses_data( __('How many related posts should be displayed in the single post?', 'stevenwatkins') ),
				"std" => 2,
				"hidden" => true,
				"options" => stevenwatkins_get_list_range(2,22),
				"type" => "select"
				),
			"blog_animation" => array( 
				"title" => esc_html__('Animation for posts', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select animation to show posts in the blog. Attention! Do not use any animation on pages with the "wheel to the anchor" behaviour (like a "Chess 2 columns")!', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"dependency" => array(
					'#page_template' => array('blog.php')
				),
				"std" => "none",
				"options" => stevenwatkins_get_list_animations_in(),
				"type" => "select"
				),
			"animation_on_mobile" => array( 
				"title" => esc_html__('Allow animation on mobile', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Allow extended animation effects on mobile devices', 'stevenwatkins') ),
				"std" => 'yes',
				"dependency" => array(
					'blog_animation' => array('^none')
				),
				"options" => stevenwatkins_get_list_yesno(),
				"type" => "switch"
				),
			'header_widgets_blog' => array(
				"title" => esc_html__('Header widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on the blog archive', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'sidebar_widgets_blog' => array(
				"title" => esc_html__('Sidebar widgets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select sidebar to show on the blog archive', 'stevenwatkins') ),
				"std" => 'sidebar_widgets',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'sidebar_position_blog' => array(
				"title" => esc_html__('Sidebar position', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select position to show sidebar on the blog archive', 'stevenwatkins') ),
				"refresh" => false,
				"std" => 'right',
				"options" => stevenwatkins_get_list_sidebars_positions(),
				"type" => "select"
				),
			'widgets_above_page_blog' => array(
				"title" => esc_html__('Widgets above the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_above_content_blog' => array(
				"title" => esc_html__('Widgets above the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_content_blog' => array(
				"title" => esc_html__('Widgets below the content', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			'widgets_below_page_blog' => array(
				"title" => esc_html__('Widgets below the page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'stevenwatkins') ),
				"std" => 'hide',
				"options" => array_merge(array('hide'=>esc_html__('- Select widgets -', 'stevenwatkins')), stevenwatkins_get_list_sidebars()),
				"type" => "select"
				),
			
		
		
		
			// Section 'Colors' - choose color scheme and customize separate colors from it
			'scheme' => array(
				"title" => esc_html__('* Color scheme editor', 'stevenwatkins'),
				"desc" => wp_kses_data( __("<b>Simple settings</b> - you can change only accented color, used for links, buttons and some accented areas.", 'stevenwatkins') )
						. '<br>'
						. wp_kses_data( __("<b>Advanced settings</b> - change all scheme's colors and get full control over the appearance of your site!", 'stevenwatkins') ),
				"priority" => 1000,
				"type" => "section"
				),
		
			'color_settings' => array(
				"title" => esc_html__('Color settings', 'stevenwatkins'),
				"desc" => '',
				"std" => 'simple',
				"options" => array(
					"simple"  => esc_html__("Simple", 'stevenwatkins'),
					"advanced" => esc_html__("Advanced", 'stevenwatkins')
				),
				"refresh" => false,
				"type" => "switch"
				),
		
			'color_scheme_editor' => array(
				"title" => esc_html__('Color Scheme', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select color scheme to edit colors', 'stevenwatkins') ),
				"std" => 'default',
				"options" => stevenwatkins_get_list_schemes(),
				"refresh" => false,
				"type" => "select"
				),
		
			'scheme_storage' => array(
				"title" => esc_html__('Colors storage', 'stevenwatkins'),
				"desc" => esc_html__('Hidden storage of the all color from the all color shemes (only for internal usage)', 'stevenwatkins'),
				"std" => '',
				"refresh" => false,
				"type" => "hidden"
				),
		
			'scheme_info_single' => array(
				"title" => esc_html__('Colors for single post/page', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify colors for single post/page (not for alter blocks)', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
				
			'bg_color' => array(
				"title" => esc_html__('Background color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Background color of the whole page', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'bd_color' => array(
				"title" => esc_html__('Border color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the bordered elements, separators, etc.', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'text' => array(
				"title" => esc_html__('Text', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Plain text color on single page/post', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_light' => array(
				"title" => esc_html__('Light text', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the post meta: post date and author, comments number, etc.', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_dark' => array(
				"title" => esc_html__('Dark text', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the headers, strong text, etc.', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_link' => array(
				"title" => esc_html__('Links', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of links and accented areas', 'stevenwatkins') ),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_hover' => array(
				"title" => esc_html__('Links hover', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Hover color for links and accented areas', 'stevenwatkins') ),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_alter' => array(
				"title" => esc_html__('Colors for alternative blocks', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify colors for alternative blocks - rectangular blocks with its own background color (posts in homepage, blog archive, search results, widgets on sidebar, footer, etc.)', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'alter_bg_color' => array(
				"title" => esc_html__('Alter background color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Background color of the alternative blocks', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bg_hover' => array(
				"title" => esc_html__('Alter hovered background color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Background color for the hovered state of the alternative blocks', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bd_color' => array(
				"title" => esc_html__('Alternative border color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Border color of the alternative blocks', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bd_hover' => array(
				"title" => esc_html__('Alternative hovered border color', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Border color for the hovered state of the alter blocks', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_text' => array(
				"title" => esc_html__('Alter text', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Text color of the alternative blocks', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_light' => array(
				"title" => esc_html__('Alter light', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the info blocks inside block with alternative background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_dark' => array(
				"title" => esc_html__('Alter dark', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the headers inside block with alternative background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_link' => array(
				"title" => esc_html__('Alter link', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the links inside block with alternative background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_hover' => array(
				"title" => esc_html__('Alter hover', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the hovered links inside block with alternative background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_input' => array(
				"title" => esc_html__('Colors for the form fields', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify colors for the form fields and textareas', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'input_bg_color' => array(
				"title" => esc_html__('Inactive background', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Background color of the inactive form fields', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bg_hover' => array(
				"title" => esc_html__('Active background', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Background color of the focused form fields', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bd_color' => array(
				"title" => esc_html__('Inactive border', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the border in the inactive form fields', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bd_hover' => array(
				"title" => esc_html__('Active border', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the border in the focused form fields', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_text' => array(
				"title" => esc_html__('Inactive field', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the text in the inactive fields', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_light' => array(
				"title" => esc_html__('Disabled field', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the disabled field', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_dark' => array(
				"title" => esc_html__('Active field', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the active field', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_inverse' => array(
				"title" => esc_html__('Colors for inverse blocks', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify colors for inverse blocks, rectangular blocks with background color equal to the links color or one of accented colors (if used in the current theme)', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'inverse_text' => array(
				"title" => esc_html__('Inverse text', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the text inside block with accented background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_light' => array(
				"title" => esc_html__('Inverse light', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the info blocks inside block with accented background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_dark' => array(
				"title" => esc_html__('Inverse dark', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the headers inside block with accented background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_link' => array(
				"title" => esc_html__('Inverse link', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the links inside block with accented background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_hover' => array(
				"title" => esc_html__('Inverse hover', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Color of the hovered links inside block with accented background', 'stevenwatkins') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'accent' => array(
				"title" => esc_html__('Accent color 1', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Accent color', 'stevenwatkins') ),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'accent2' => array(
				"title" => esc_html__('Accent color 2', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Accent color', 'stevenwatkins') ),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'accent3' => array(
				"title" => esc_html__('Accent color 3', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Accent color', 'stevenwatkins') ),
				"std" => '$stevenwatkins_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),

			// Section 'Hidden' - internal options
			'media_title' => array(
				"title" => esc_html__('Media title', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Used as title for the audio and video item in this post', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'post',
					'section' => esc_html__('Title', 'stevenwatkins')
				),
				"hidden" => true,
				"std" => '',
				"type" => "text"
				),
			'media_author' => array(
				"title" => esc_html__('Media author', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Used as author name for the audio and video item in this post', 'stevenwatkins') ),
				"override" => array(
					'mode' => 'post',
					'section' => esc_html__('Title', 'stevenwatkins')
				),
				"hidden" => true,
				"std" => '',
				"type" => "text"
				),

			// Internal options.
			// Attention! Don't change any options in the section below!
			'reset_options' => array(
				"title" => '',
				"desc" => '',
				"std" => '0',
				"type" => "hidden",
				),

		));


		// Prepare panel 'Fonts'
		$fonts = array(
		
			// Panel 'Fonts' - manage fonts loading and set parameters of the base theme elements
			'fonts' => array(
				"title" => esc_html__('* Fonts settings', 'stevenwatkins'),
				"desc" => '',
				"priority" => 1500,
				"type" => "panel"
				),

			// Section 'Load_fonts'
			'load_fonts' => array(
				"title" => esc_html__('Load fonts', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify fonts to load when theme start. You can use them in the base theme elements: headers, text, menu, links, input fields, etc.', 'stevenwatkins') )
						. '<br>'
						. wp_kses_data( __('<b>Attention!</b> Press "Refresh" button to reload preview area after the all fonts are changed', 'stevenwatkins') ),
				"type" => "section"
				),
			'load_fonts_subset' => array(
				"title" => esc_html__('Google fonts subsets', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Specify comma separated list of the subsets which will be load from Google fonts', 'stevenwatkins') )
						. '<br>'
						. wp_kses_data( __('Available subsets are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese', 'stevenwatkins') ),
				"refresh" => false,
				"std" => '$stevenwatkins_get_load_fonts_subset',
				"type" => "text"
				)
		);

		for ($i=1; $i<=stevenwatkins_get_theme_setting('max_load_fonts'); $i++) {
			$fonts["load_fonts-{$i}-info"] = array(
				"title" => esc_html(sprintf(__('Font %s', 'stevenwatkins'), $i)),
				"desc" => '',
				"type" => "info",
				);
			$fonts["load_fonts-{$i}-name"] = array(
				"title" => esc_html__('Font name', 'stevenwatkins'),
				"desc" => '',
				"refresh" => false,
				"std" => '$stevenwatkins_get_load_fonts_option',
				"type" => "text"
				);
			$fonts["load_fonts-{$i}-family"] = array(
				"title" => esc_html__('Font family', 'stevenwatkins'),
				"desc" => $i==1 
							? wp_kses_data( __('Select font family to use it if font above is not available', 'stevenwatkins') )
							: '',
				"refresh" => false,
				"std" => '$stevenwatkins_get_load_fonts_option',
				"options" => array(
					'inherit' => esc_html__("Inherit", 'stevenwatkins'),
					'serif' => esc_html__('serif', 'stevenwatkins'),
					'sans-serif' => esc_html__('sans-serif', 'stevenwatkins'),
					'monospace' => esc_html__('monospace', 'stevenwatkins'),
					'cursive' => esc_html__('cursive', 'stevenwatkins'),
					'fantasy' => esc_html__('fantasy', 'stevenwatkins')
				),
				"type" => "select"
				);
			$fonts["load_fonts-{$i}-styles"] = array(
				"title" => esc_html__('Font styles', 'stevenwatkins'),
				"desc" => $i==1 
							? wp_kses_data( __('Font styles used only for the Google fonts. This is a comma separated list of the font weight and styles. For example: 400,400italic,400', 'stevenwatkins') )
											. '<br>'
								. wp_kses_data( __('<b>Attention!</b> Each weight and style increase download size! Specify only used weights and styles.', 'stevenwatkins') )
							: '',
				"refresh" => false,
				"std" => '$stevenwatkins_get_load_fonts_option',
				"type" => "text"
				);
		}
		$fonts['load_fonts_end'] = array(
			"type" => "section_end"
			);

		// Sections with font's attributes for each theme element
		$theme_fonts = stevenwatkins_get_theme_fonts();
		foreach ($theme_fonts as $tag=>$v) {
			$fonts["{$tag}_section"] = array(
				"title" => !empty($v['title']) 
								? $v['title'] 
								: esc_html(sprintf(__('%s settings', 'stevenwatkins'), $tag)),
				"desc" => !empty($v['description']) 
								? $v['description'] 
								: wp_kses_post( sprintf(__('Font settings of the "%s" tag.', 'stevenwatkins'), $tag) ),
				"type" => "section",
				);
	
			foreach ($v as $css_prop=>$css_value) {
				if (in_array($css_prop, array('title', 'description'))) continue;
				$options = '';
				$type = 'text';
				$title = ucfirst(str_replace('-', ' ', $css_prop));
				if ($css_prop == 'font-family') {
					$type = 'select';
					$options = stevenwatkins_get_list_load_fonts(true);
				} else if ($css_prop == 'font-weight') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'stevenwatkins'),
						'100' => esc_html__('100 (Light)', 'stevenwatkins'),
						'200' => esc_html__('200 (Light)', 'stevenwatkins'),
						'300' => esc_html__('300 (Thin)',  'stevenwatkins'),
						'400' => esc_html__('400 (Normal)', 'stevenwatkins'),
						'500' => esc_html__('500 (Semibold)', 'stevenwatkins'),
						'600' => esc_html__('600 (Semibold)', 'stevenwatkins'),
						'700' => esc_html__('700 (Bold)', 'stevenwatkins'),
						'800' => esc_html__('800 (Black)', 'stevenwatkins'),
						'900' => esc_html__('900 (Black)', 'stevenwatkins')
					);
				} else if ($css_prop == 'font-style') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'stevenwatkins'),
						'normal' => esc_html__('Normal', 'stevenwatkins'), 
						'italic' => esc_html__('Italic', 'stevenwatkins')
					);
				} else if ($css_prop == 'text-decoration') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'stevenwatkins'),
						'none' => esc_html__('None', 'stevenwatkins'), 
						'underline' => esc_html__('Underline', 'stevenwatkins'),
						'overline' => esc_html__('Overline', 'stevenwatkins'),
						'line-through' => esc_html__('Line-through', 'stevenwatkins')
					);
				} else if ($css_prop == 'text-transform') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'stevenwatkins'),
						'none' => esc_html__('None', 'stevenwatkins'), 
						'uppercase' => esc_html__('Uppercase', 'stevenwatkins'),
						'lowercase' => esc_html__('Lowercase', 'stevenwatkins'),
						'capitalize' => esc_html__('Capitalize', 'stevenwatkins')
					);
				}
				$fonts["{$tag}_{$css_prop}"] = array(
					"title" => $title,
					"desc" => '',
					"refresh" => false,
					"std" => '$stevenwatkins_get_theme_fonts_option',
					"options" => $options,
					"type" => $type
				);
			}
			
			$fonts["{$tag}_section_end"] = array(
				"type" => "section_end"
				);
		}

		$fonts['fonts_end'] = array(
			"type" => "panel_end"
			);

		// Add fonts parameters into Theme Options
		stevenwatkins_storage_merge_array('options', '', $fonts);
	}
}




// -----------------------------------------------------------------
// -- Create and manage Theme Options
// -----------------------------------------------------------------

// Theme init priorities:
// 2 - create Theme Options
if (!function_exists('stevenwatkins_options_theme_setup2')) {
	add_action( 'after_setup_theme', 'stevenwatkins_options_theme_setup2', 2 );
	function stevenwatkins_options_theme_setup2() {
		stevenwatkins_options_create();
	}
}

// Step 1: Load default settings and previously saved mods
if (!function_exists('stevenwatkins_options_theme_setup5')) {
	add_action( 'after_setup_theme', 'stevenwatkins_options_theme_setup5', 5 );
	function stevenwatkins_options_theme_setup5() {
		stevenwatkins_storage_set('options_reloaded', false);
		stevenwatkins_load_theme_options();
	}
}

// Step 2: Load current theme customization mods
if (is_customize_preview()) {
	if (!function_exists('stevenwatkins_load_custom_options')) {
		add_action( 'wp_loaded', 'stevenwatkins_load_custom_options' );
		function stevenwatkins_load_custom_options() {
			if (!stevenwatkins_storage_get('options_reloaded')) {
				stevenwatkins_storage_set('options_reloaded', true);
				stevenwatkins_load_theme_options();
			}
		}
	}
}

// Load current values for each customizable option
if ( !function_exists('stevenwatkins_load_theme_options') ) {
	function stevenwatkins_load_theme_options() {
		$options = stevenwatkins_storage_get('options');
		$reset = (int) get_theme_mod('reset_options', 0);
		foreach ($options as $k=>$v) {
			if (isset($v['std'])) {
				if (strpos($v['std'], '$stevenwatkins_')!==false) {
					$func = substr($v['std'], 1);
					if (function_exists($func)) {
						$v['std'] = $func($k);
					}
				}
				$value = $v['std'];
				if (!$reset) {
					if (isset($_GET[$k]))
						$value = $_GET[$k];
					else {
						$tmp = get_theme_mod($k, -987654321);
						if ($tmp != -987654321) $value = $tmp;
					}
				}
				stevenwatkins_storage_set_array2('options', $k, 'val', $value);
				if ($reset) remove_theme_mod($k);
			}
		}
		if ($reset) {
			// Unset reset flag
			set_theme_mod('reset_options', 0);
			// Regenerate CSS with default colors and fonts
			stevenwatkins_customizer_save_css();
		} else {
			do_action('stevenwatkins_action_load_options');
		}
	}
}

// Override options with stored page/post meta
if ( !function_exists('stevenwatkins_override_theme_options') ) {
	add_action( 'wp', 'stevenwatkins_override_theme_options', 1 );
	function stevenwatkins_override_theme_options($query=null) {
		if (is_page_template('blog.php')) {
			stevenwatkins_storage_set('blog_archive', true);
			stevenwatkins_storage_set('blog_template', get_the_ID());
		}
		stevenwatkins_storage_set('blog_mode', stevenwatkins_detect_blog_mode());
		if (is_singular()) {
			stevenwatkins_storage_set('options_meta', get_post_meta(get_the_ID(), 'stevenwatkins_options', true));
		}
	}
}


// Return customizable option value
if (!function_exists('stevenwatkins_get_theme_option')) {
	function stevenwatkins_get_theme_option($name, $defa='', $strict_mode=false, $post_id=0) {
		$rez = $defa;
		$from_post_meta = false;
		if ($post_id > 0) {
			if (!stevenwatkins_storage_isset('post_options_meta', $post_id))
				stevenwatkins_storage_set_array('post_options_meta', $post_id, get_post_meta($post_id, 'stevenwatkins_options', true));
			if (stevenwatkins_storage_isset('post_options_meta', $post_id, $name)) {
				$tmp = stevenwatkins_storage_get_array('post_options_meta', $post_id, $name);
				if (!stevenwatkins_is_inherit($tmp)) {
					$rez = $tmp;
					$from_post_meta = true;
				}
			}
		}
		if (!$from_post_meta && stevenwatkins_storage_isset('options')) {
			if ( !stevenwatkins_storage_isset('options', $name) ) {
				$rez = $tmp = '_not_exists_';
				if (function_exists('trx_addons_get_option'))
					$rez = trx_addons_get_option($name, $tmp, false);
				if ($rez === $tmp) {
					if ($strict_mode) {
						$s = debug_backtrace();
						//array_shift($s);
						$s = array_shift($s);
						echo '<pre>' . sprintf(esc_html__('Undefined option "%s" called from:', 'stevenwatkins'), $name);
						if (function_exists('dco')) dco($s);
						else print_r($s);
						echo '</pre>';
						die();
					} else
						$rez = $defa;
				}
			} else {
				$blog_mode = stevenwatkins_storage_get('blog_mode');
				// Override option from GET or POST for current blog mode
				if (!empty($blog_mode) && isset($_REQUEST[$name . '_' . $blog_mode])) {
					$rez = $_REQUEST[$name . '_' . $blog_mode];
				// Override option from GET
				} else if (isset($_REQUEST[$name])) {
					$rez = $_REQUEST[$name];
				// Override option from current page settings (if exists)
				} else if (stevenwatkins_storage_isset('options_meta', $name) && !stevenwatkins_is_inherit(stevenwatkins_storage_get_array('options_meta', $name))) {
					$rez = stevenwatkins_storage_get_array('options_meta', $name);
				// Override option from current blog mode settings: 'home', 'search', 'page', 'post', 'blog', etc. (if exists)
				} else if (!empty($blog_mode) && stevenwatkins_storage_isset('options', $name . '_' . $blog_mode, 'val') && !stevenwatkins_is_inherit(stevenwatkins_storage_get_array('options', $name . '_' . $blog_mode, 'val'))) {
					$rez = stevenwatkins_storage_get_array('options', $name . '_' . $blog_mode, 'val');
				// Get saved option value
				} else if (stevenwatkins_storage_isset('options', $name, 'val')) {
					$rez = stevenwatkins_storage_get_array('options', $name, 'val');
				// Get ThemeREX Addons option value
				} else if (function_exists('trx_addons_get_option')) {
					$rez = trx_addons_get_option($name, $defa, false);
				}
			}
		}
		return $rez;
	}
}


// Check if customizable option exists
if (!function_exists('stevenwatkins_check_theme_option')) {
	function stevenwatkins_check_theme_option($name) {
		return stevenwatkins_storage_isset('options', $name);
	}
}

// Get dependencies list from the Theme Options
if ( !function_exists('stevenwatkins_get_theme_dependencies') ) {
	function stevenwatkins_get_theme_dependencies() {
		$options = stevenwatkins_storage_get('options');
		$depends = array();
		foreach ($options as $k=>$v) {
			if (isset($v['dependency'])) 
				$depends[$k] = $v['dependency'];
		}
		return $depends;
	}
}

// Return internal theme setting value
if (!function_exists('stevenwatkins_get_theme_setting')) {
	function stevenwatkins_get_theme_setting($name) {
		return stevenwatkins_storage_isset('settings', $name) ? stevenwatkins_storage_get_array('settings', $name) : false;
	}
}


// Set theme setting
if ( !function_exists( 'stevenwatkins_set_theme_setting' ) ) {
	function stevenwatkins_set_theme_setting($option_name, $value) {
		if (stevenwatkins_storage_isset('settings', $option_name))
			stevenwatkins_storage_set_array('settings', $option_name, $value);
	}
}
?>