<?php
/**
 * The Gallery template to display posts
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_blog_style = explode('_', stevenwatkins_get_theme_option('blog_style'));
$stevenwatkins_columns = empty($stevenwatkins_blog_style[1]) ? 2 : max(2, $stevenwatkins_blog_style[1]);
$stevenwatkins_post_format = get_post_format();
$stevenwatkins_post_format = empty($stevenwatkins_post_format) ? 'standard' : str_replace('post-format-', '', $stevenwatkins_post_format);
$stevenwatkins_animation = stevenwatkins_get_theme_option('blog_animation');
$stevenwatkins_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_gallery post_layout_gallery_'.esc_attr($stevenwatkins_columns).' post_format_'.esc_attr($stevenwatkins_post_format) ); ?>
	<?php echo (!stevenwatkins_is_off($stevenwatkins_animation) ? ' data-animation="'.esc_attr(stevenwatkins_get_animation_classes($stevenwatkins_animation)).'"' : ''); ?>
	data-size="<?php if (!empty($stevenwatkins_image[1]) && !empty($stevenwatkins_image[2])) echo intval($stevenwatkins_image[1]) .'x' . intval($stevenwatkins_image[2]); ?>"
	data-src="<?php if (!empty($stevenwatkins_image[0])) echo esc_url($stevenwatkins_image[0]); ?>"
	>

	<?php
	$stevenwatkins_image_hover = 'icon';
	if (in_array($stevenwatkins_image_hover, array('icons', 'zoom'))) $stevenwatkins_image_hover = 'dots';
	// Featured image
	stevenwatkins_show_post_featured(array(
		'hover' => $stevenwatkins_image_hover,
		'thumb_size' => stevenwatkins_get_thumb_size( strpos(stevenwatkins_get_theme_option('body_style'), 'full')!==false || $stevenwatkins_columns < 3 ? 'masonry-big' : 'masonry' ),
		'thumb_only' => true,
		'show_no_image' => true,
		'post_info' => '<div class="post_details">'
							. '<h2 class="post_title"><a href="'.esc_url(get_permalink()).'">'. esc_html(get_the_title()) . '</a></h2>'
							. '<div class="post_description">'
								. stevenwatkins_show_post_meta(array(
									'categories' => true,
									'date' => true,
									'edit' => false,
									'seo' => false,
									'share' => true,
									'counters' => 'comments',
									'echo' => false
									))
								. '<div class="post_description_content">'
									. apply_filters('the_excerpt', get_the_excerpt())
								. '</div>'
								. '<a href="'.esc_url(get_permalink()).'" class="theme_button post_readmore"><span class="post_readmore_label">' . esc_html__('Learn more', 'stevenwatkins') . '</span></a>'
							. '</div>'
						. '</div>'
	));
	?>
</article>