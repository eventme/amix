<?php
/**
 * The Sticky template for displaying sticky posts
 *
 * Used for index/archive
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_columns = max(1, min(3, count(get_option( 'sticky_posts' ))));
$stevenwatkins_post_format = get_post_format();
$stevenwatkins_post_format = empty($stevenwatkins_post_format) ? 'standard' : str_replace('post-format-', '', $stevenwatkins_post_format);
$stevenwatkins_animation = stevenwatkins_get_theme_option('blog_animation');

?><div class="column-1_<?php echo esc_attr($stevenwatkins_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_sticky post_format_'.esc_attr($stevenwatkins_post_format) ); ?>
	<?php echo (!stevenwatkins_is_off($stevenwatkins_animation) ? ' data-animation="'.esc_attr(stevenwatkins_get_animation_classes($stevenwatkins_animation)).'"' : ''); ?>
	>

	<?php
	if ( is_sticky() && is_home() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	stevenwatkins_show_post_featured(array(
		'thumb_size' => stevenwatkins_get_thumb_size($stevenwatkins_columns==1 ? 'big' : ($stevenwatkins_columns==2 ? 'med' : 'avatar'))
	));

	if ( !in_array($stevenwatkins_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php
			// Post title
			the_title( sprintf( '<h6 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h6>' );
			// Post meta
			stevenwatkins_show_post_meta();
			?>
		</div><!-- .entry-header -->
		<?php
	}
	?>
</article></div>