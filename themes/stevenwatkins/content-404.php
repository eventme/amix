<article <?php post_class( 'post_item_single post_item_404' ); ?>>
	<div class="post_content">
		<h1 class="page_title"><?php esc_html_e( '404', 'stevenwatkins' ); ?></h1>
		<div class="page_info">
			<h1 class="page_subtitle"><?php esc_html_e('Oops...', 'stevenwatkins'); ?></h1>
			<p class="page_description"><?php echo wp_kses_post(__("We're sorry, but <br>something went wrong.", 'stevenwatkins')); ?></p>
			<div class="sc_item_button sc_button_wrap">
				<a href="<?php echo esc_url(home_url('/')); ?>" class="go_home sc_button sc_button_accent_bg_1 sc_button_size_large sc_button_icon_right">
					<span class="sc_button_icon">
						<span class="icon-move-right-theme"></span>
					</span>
					<span class="sc_button_text">
						<span class="sc_button_title"><?php esc_html_e('Homepage', 'stevenwatkins'); ?></span>
					</span>
				</a>
			</div>
		</div>
	</div>
</article>
