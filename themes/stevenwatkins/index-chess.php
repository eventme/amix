<?php
/**
 * The template for homepage posts with "Chess" style
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

stevenwatkins_storage_set('blog_archive', true);

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$stevenwatkins_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$stevenwatkins_sticky_out = is_array($stevenwatkins_stickies) && count($stevenwatkins_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($stevenwatkins_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$stevenwatkins_sticky_out) {
		?><div class="chess_wrap posts_container"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($stevenwatkins_sticky_out && !is_sticky()) {
			$stevenwatkins_sticky_out = false;
			?></div><div class="chess_wrap posts_container"><?php
		}
		get_template_part( 'content', $stevenwatkins_sticky_out && is_sticky() ? 'sticky' :'chess' );
	}
	
	?></div><?php

	stevenwatkins_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>