<?php
/**
 * The template to display Admin notices
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0.1
 */
?>
<div class="update-nag" id="stevenwatkins_admin_notice">
	<h3 class="stevenwatkins_notice_title"><?php echo sprintf(esc_html__('Welcome to %s', 'stevenwatkins'), wp_get_theme()->name); ?></h3>
	<?php if (!stevenwatkins_exists_trx_addons()) { ?>
	<p><?php echo wp_kses_data(__('<b>Attention!</b> Plugin "ThemeREX Addons is required! Please, install and activate it!', 'stevenwatkins')); ?></p>
	<?php } ?>
	<p>
		<?php if (stevenwatkins_get_value_gp('page')!='tgmpa-install-plugins') { ?>
		<a href="<?php echo esc_url(admin_url().'themes.php?page=tgmpa-install-plugins'); ?>" class="button-primary"><i class="dashicons dashicons-admin-plugins"></i> <?php esc_html_e('Install plugins', 'stevenwatkins'); ?></a>
		<?php } ?>
        <a href="<?php echo esc_url(admin_url().'customize.php'); ?>" class="button-primary"><i class="dashicons dashicons-admin-appearance"></i> <?php esc_html_e('Theme Customizer', 'stevenwatkins'); ?></a>
        <a href="#" class="button stevenwatkins_hide_notice"><i class="dashicons dashicons-dismiss"></i> <?php esc_html_e('Hide Notice', 'stevenwatkins'); ?></a>
	</p>
</div>