<?php
/**
 * The template for displaying main menu
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */
$stevenwatkins_header_image = get_query_var('stevenwatkins_header_image');
?>
<div class="top_panel_fixed_wrap"></div>
<div class="top_panel_navi 
			<?php if ($stevenwatkins_header_image!='') echo ' with_bg_image'; ?>
			scheme_<?php echo esc_attr(stevenwatkins_is_inherit(stevenwatkins_get_theme_option('menu_scheme')) 
												? (stevenwatkins_is_inherit(stevenwatkins_get_theme_option('header_scheme')) 
													? stevenwatkins_get_theme_option('color_scheme') 
													: stevenwatkins_get_theme_option('header_scheme')) 
												: stevenwatkins_get_theme_option('menu_scheme')); ?>">
	<div class="menu_main_wrap clearfix menu_hover_<?php echo esc_attr(stevenwatkins_get_theme_option('menu_hover')); ?>">
		<div class="content_wrap">
			<?php
			// Filter header components
			$stevenwatkins_header_parts = apply_filters('stevenwatkins_filter_header_parts', array(
				'logo' => true,
				'menu' => true,
				'search' => true
				),
				'menu_main');
			
			// Logo
			if (!empty($stevenwatkins_header_parts['logo'])) {
				get_template_part( 'templates/header-logo' );
			}
			
			// Main menu
			if (!empty($stevenwatkins_header_parts['menu'])) {
				$stevenwatkins_stevenwatkins_menu_main = stevenwatkins_get_nav_menu('menu_main');
				if (empty($stevenwatkins_stevenwatkins_menu_main)) $stevenwatkins_stevenwatkins_menu_main = stevenwatkins_get_nav_menu();
				stevenwatkins_show_layout($stevenwatkins_stevenwatkins_menu_main);
				// Store menu layout for the mobile menu
				set_query_var('stevenwatkins_menu_main', $stevenwatkins_stevenwatkins_menu_main);
			}

			// Display search field
//			if (!empty($stevenwatkins_header_parts['search'])) {
//				set_query_var('stevenwatkins_search_in_header', true);
//				get_template_part( 'templates/search-field' );
//			}
			?>
		<div class="amix_right_header">
			<div class="dropdown">
					<a href="tel:380 63 098 85 12">+380 63 098 85 12</a>
						<div class="dropdown-content">
							<a href="tel:380 63 098 85 12">+380 63 098 85 12</a>
							<a href="tel:380 63 098 85 12">+380 63 098 85 12</a>
						</div>
			</div>
				<div class="curt_amix"><a href="<?php echo wc_get_cart_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/curt.svg" alt="Curt"></a></div>
			<div class="login_amix">
				<?php if ( is_user_logged_in() ) { ?>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/user.svg" alt="Login"><?php _e('Мой аккаунт','woothemes'); ?></a></a>
				<?php }
				else { ?>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/user.svg" alt="Login"><?php _e('Войти / Регистрация','woothemes'); ?></a></a>
				<?php } ?></div>
			</div>
		</div>
	</div>
</div><!-- /.top_panel_navi -->