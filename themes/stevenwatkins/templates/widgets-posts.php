<?php
/**
 * The template for displaying posts in widgets and/or in the search results
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

$stevenwatkins_post_id    = get_the_ID();
$stevenwatkins_post_date  = stevenwatkins_get_date();
$stevenwatkins_post_title = get_the_title();
$stevenwatkins_post_link  = get_permalink();
$stevenwatkins_post_author_id   = get_the_author_meta('ID');
$stevenwatkins_post_author_name = get_the_author_meta('display_name');
$stevenwatkins_post_author_url  = get_author_posts_url($stevenwatkins_post_author_id, '');

$stevenwatkins_args = get_query_var('stevenwatkins_args_widgets_posts');
$stevenwatkins_show_date = isset($stevenwatkins_args['show_date']) ? (int) $stevenwatkins_args['show_date'] : 1;
$stevenwatkins_show_image = isset($stevenwatkins_args['show_image']) ? (int) $stevenwatkins_args['show_image'] : 1;
$stevenwatkins_show_author = isset($stevenwatkins_args['show_author']) ? (int) $stevenwatkins_args['show_author'] : 1;
$stevenwatkins_show_counters = isset($stevenwatkins_args['show_counters']) ? (int) $stevenwatkins_args['show_counters'] : 1;
$stevenwatkins_show_categories = isset($stevenwatkins_args['show_categories']) ? (int) $stevenwatkins_args['show_categories'] : 1;

$stevenwatkins_output = stevenwatkins_storage_get('stevenwatkins_output_widgets_posts');

$stevenwatkins_post_counters_output = '';
if ( $stevenwatkins_show_counters ) {
	$stevenwatkins_post_counters_output = '<span class="post_info_item post_info_counters">'
								. stevenwatkins_get_post_counters('comments')
							. '</span>';
}


$stevenwatkins_output .= '<article class="post_item with_thumb">';

if ($stevenwatkins_show_image) {
	$stevenwatkins_post_thumb = get_the_post_thumbnail($stevenwatkins_post_id, stevenwatkins_get_thumb_size('tiny'), array(
		'alt' => get_the_title()
	));
	if ($stevenwatkins_post_thumb) $stevenwatkins_output .= '<div class="post_thumb">' . ($stevenwatkins_post_link ? '<a href="' . esc_url($stevenwatkins_post_link) . '">' : '') . ($stevenwatkins_post_thumb) . ($stevenwatkins_post_link ? '</a>' : '') . '</div>';
}

$stevenwatkins_output .= '<div class="post_content">'
			. ($stevenwatkins_show_categories
					? '<div class="post_categories">'
						. stevenwatkins_get_post_categories()
						. $stevenwatkins_post_counters_output
						. '</div>'
					: '')
			. '<h6 class="post_title">' . ($stevenwatkins_post_link ? '<a href="' . esc_url($stevenwatkins_post_link) . '">' : '') . ($stevenwatkins_post_title) . ($stevenwatkins_post_link ? '</a>' : '') . '</h6>'
			. apply_filters('stevenwatkins_filter_get_post_info',
								'<div class="post_info">'
									. ($stevenwatkins_show_date
										? '<span class="post_info_item post_info_posted">'
											. ($stevenwatkins_post_link ? '<a href="' . esc_url($stevenwatkins_post_link) . '" class="post_info_date">' : '')
											. esc_html($stevenwatkins_post_date)
											. ($stevenwatkins_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. ($stevenwatkins_show_author
										? '<span class="post_info_item post_info_posted_by">'
											. esc_html__('by', 'stevenwatkins') . ' '
											. ($stevenwatkins_post_link ? '<a href="' . esc_url($stevenwatkins_post_author_url) . '" class="post_info_author">' : '')
											. esc_html($stevenwatkins_post_author_name)
											. ($stevenwatkins_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. (!$stevenwatkins_show_categories && $stevenwatkins_post_counters_output
										? $stevenwatkins_post_counters_output
										: '')
								. '</div>')
		. '</div>'
	. '</article>';
stevenwatkins_storage_set('stevenwatkins_output_widgets_posts', $stevenwatkins_output);
?>