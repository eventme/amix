<?php
/**
 * The template for displaying Logo or Site name and slogan in the Header
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Site logo
$stevenwatkins_logo_image = '';
if (stevenwatkins_get_retina_multiplier(2) > 1)
	$stevenwatkins_logo_image = stevenwatkins_get_theme_option( 'logo_retina' );
if (empty($stevenwatkins_logo_image)) 
	$stevenwatkins_logo_image = stevenwatkins_get_theme_option( 'logo' );
$stevenwatkins_logo_text   = get_bloginfo( 'name' );
$stevenwatkins_logo_slogan = get_bloginfo( 'description', 'display' );
if (!empty($stevenwatkins_logo_image) || !empty($stevenwatkins_logo_text)) {
	?><a class="logo" href="<?php echo is_front_page() ? '#' : esc_url(home_url('/')); ?>"><?php
		if (!empty($stevenwatkins_logo_image)) {
			$stevenwatkins_attr = stevenwatkins_getimagesize($stevenwatkins_logo_image);
			echo '<img src="'.esc_url($stevenwatkins_logo_image).'" class="logo_main" alt=""'.(!empty($stevenwatkins_attr[3]) ? sprintf(' %s', $stevenwatkins_attr[3]) : '').'>' ;
		} else {
			stevenwatkins_show_layout(stevenwatkins_prepare_macros($stevenwatkins_logo_text), '<span class="logo_text">', '</span>');
			stevenwatkins_show_layout(stevenwatkins_prepare_macros($stevenwatkins_logo_slogan), '<span class="logo_slogan">', '</span>');
		}
	?></a><?php
}
?>