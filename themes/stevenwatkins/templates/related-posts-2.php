<?php
/**
 * The template 'Style 2' to displaying related posts
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Thumb image
$stevenwatkins_thumb_image = has_post_thumbnail() 
			? wp_get_attachment_image_src(get_post_thumbnail_id(), stevenwatkins_get_thumb_size('medium')) 
			: ( (float) wp_get_theme()->get('Version') > 1.0
					? stevenwatkins_get_no_image_placeholder()
					: ''
				);
if (is_array($stevenwatkins_thumb_image)) $stevenwatkins_thumb_image = $stevenwatkins_thumb_image[0];
$stevenwatkins_link = get_permalink();
$stevenwatkins_hover = stevenwatkins_get_theme_option('image_hover');
?>
<div class="related_item related_item_style_2">
	<?php if (!empty($stevenwatkins_thumb_image)) { ?>
		<div class="post_featured<?php
					if (has_post_thumbnail()) echo ' hover_'.esc_attr($stevenwatkins_hover); 
					echo ' ' . esc_attr(stevenwatkins_add_inline_style('background-image:url('.esc_url($stevenwatkins_thumb_image).');'));
					?>">
			<?php
			if (has_post_thumbnail()) {
				?><div class="mask"></div><?php
				stevenwatkins_hovers_add_icons($stevenwatkins_hover);
			}
			?>
		</div>
	<?php } ?>
	<div class="post_header entry-header">
		<?php
		if ( in_array(get_post_type(), array( 'post', 'attachment' ) ) ) {
			?><span class="post_date"><a href="<?php echo esc_url($stevenwatkins_link); ?>"><?php echo stevenwatkins_get_date(); ?></a></span><?php
		}
		?>
		<h6 class="post_title entry-title"><a href="<?php echo esc_url($stevenwatkins_link); ?>"><?php echo the_title(); ?></a></h6>
	</div>
</div>
