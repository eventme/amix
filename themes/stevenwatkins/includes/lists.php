<?php
/**
 * Theme lists
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }



// Return numbers range
if ( !function_exists( 'stevenwatkins_get_list_range' ) ) {
	function stevenwatkins_get_list_range($from=1, $to=2, $prepend_inherit=false) {
		$list = array();
		for ($i=$from; $i<=$to; $i++)
			$list[$i] = $i;
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}



// Return styles list
if ( !function_exists( 'stevenwatkins_get_list_styles' ) ) {
	function stevenwatkins_get_list_styles($from=1, $to=2, $prepend_inherit=false) {
		$list = array();
		for ($i=$from; $i<=$to; $i++)
			$list[$i] = sprintf(esc_html__('Style %d', 'stevenwatkins'), $i);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list with 'Yes' and 'No' items
if ( !function_exists( 'stevenwatkins_get_list_yesno' ) ) {
	function stevenwatkins_get_list_yesno($prepend_inherit=false) {
		$list = array(
			"yes"	=> esc_html__("Yes", 'stevenwatkins'),
			"no"	=> esc_html__("No", 'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list with 'On' and 'Of' items
if ( !function_exists( 'stevenwatkins_get_list_onoff' ) ) {
	function stevenwatkins_get_list_onoff($prepend_inherit=false) {
		$list = array(
			"on"	=> esc_html__("On", 'stevenwatkins'),
			"off"	=> esc_html__("Off", 'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list with 'Show' and 'Hide' items
if ( !function_exists( 'stevenwatkins_get_list_showhide' ) ) {
	function stevenwatkins_get_list_showhide($prepend_inherit=false) {
		$list = array(
			"show" => esc_html__("Show", 'stevenwatkins'),
			"hide" => esc_html__("Hide", 'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list with 'Horizontal' and 'Vertical' items
if ( !function_exists( 'stevenwatkins_get_list_directions' ) ) {
	function stevenwatkins_get_list_directions($prepend_inherit=false) {
		$list = array(
			"horizontal" => esc_html__("Horizontal", 'stevenwatkins'),
			"vertical"   => esc_html__("Vertical", 'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list of the animations
if ( !function_exists( 'stevenwatkins_get_list_animations' ) ) {
	function stevenwatkins_get_list_animations($prepend_inherit=false) {
		$list = array(
			'none'			=> esc_html__('- None -',	'stevenwatkins'),
			'bounced'		=> esc_html__('Bounced',	'stevenwatkins'),
			'elastic'		=> esc_html__('Elastic',	'stevenwatkins'),
			'flash'			=> esc_html__('Flash',		'stevenwatkins'),
			'flip'			=> esc_html__('Flip',		'stevenwatkins'),
			'pulse'			=> esc_html__('Pulse',		'stevenwatkins'),
			'rubberBand'	=> esc_html__('Rubber Band','stevenwatkins'),
			'shake'			=> esc_html__('Shake',		'stevenwatkins'),
			'swing'			=> esc_html__('Swing',		'stevenwatkins'),
			'tada'			=> esc_html__('Tada',		'stevenwatkins'),
			'wobble'		=> esc_html__('Wobble',		'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list of the enter animations
if ( !function_exists( 'stevenwatkins_get_list_animations_in' ) ) {
	function stevenwatkins_get_list_animations_in($prepend_inherit=false) {
		$list = array(
			'none'				=> esc_html__('- None -',			'stevenwatkins'),
			'bounceIn'			=> esc_html__('Bounce In',			'stevenwatkins'),
			'bounceInUp'		=> esc_html__('Bounce In Up',		'stevenwatkins'),
			'bounceInDown'		=> esc_html__('Bounce In Down',		'stevenwatkins'),
			'bounceInLeft'		=> esc_html__('Bounce In Left',		'stevenwatkins'),
			'bounceInRight'		=> esc_html__('Bounce In Right',	'stevenwatkins'),
			'elastic'			=> esc_html__('Elastic In',			'stevenwatkins'),
			'fadeIn'			=> esc_html__('Fade In',			'stevenwatkins'),
			'fadeInUp'			=> esc_html__('Fade In Up',			'stevenwatkins'),
			'fadeInUpSmall'		=> esc_html__('Fade In Up Small',	'stevenwatkins'),
			'fadeInUpBig'		=> esc_html__('Fade In Up Big',		'stevenwatkins'),
			'fadeInDown'		=> esc_html__('Fade In Down',		'stevenwatkins'),
			'fadeInDownBig'		=> esc_html__('Fade In Down Big',	'stevenwatkins'),
			'fadeInLeft'		=> esc_html__('Fade In Left',		'stevenwatkins'),
			'fadeInLeftBig'		=> esc_html__('Fade In Left Big',	'stevenwatkins'),
			'fadeInRight'		=> esc_html__('Fade In Right',		'stevenwatkins'),
			'fadeInRightBig'	=> esc_html__('Fade In Right Big',	'stevenwatkins'),
			'flipInX'			=> esc_html__('Flip In X',			'stevenwatkins'),
			'flipInY'			=> esc_html__('Flip In Y',			'stevenwatkins'),
			'lightSpeedIn'		=> esc_html__('Light Speed In',		'stevenwatkins'),
			'rotateIn'			=> esc_html__('Rotate In',			'stevenwatkins'),
			'rotateInUpLeft'	=> esc_html__('Rotate In Down Left','stevenwatkins'),
			'rotateInUpRight'	=> esc_html__('Rotate In Up Right',	'stevenwatkins'),
			'rotateInDownLeft'	=> esc_html__('Rotate In Up Left',	'stevenwatkins'),
			'rotateInDownRight'	=> esc_html__('Rotate In Down Right','stevenwatkins'),
			'rollIn'			=> esc_html__('Roll In',			'stevenwatkins'),
			'slideInUp'			=> esc_html__('Slide In Up',		'stevenwatkins'),
			'slideInDown'		=> esc_html__('Slide In Down',		'stevenwatkins'),
			'slideInLeft'		=> esc_html__('Slide In Left',		'stevenwatkins'),
			'slideInRight'		=> esc_html__('Slide In Right',		'stevenwatkins'),
			'wipeInLeftTop'		=> esc_html__('Wipe In Left Top',	'stevenwatkins'),
			'zoomIn'			=> esc_html__('Zoom In',			'stevenwatkins'),
			'zoomInUp'			=> esc_html__('Zoom In Up',			'stevenwatkins'),
			'zoomInDown'		=> esc_html__('Zoom In Down',		'stevenwatkins'),
			'zoomInLeft'		=> esc_html__('Zoom In Left',		'stevenwatkins'),
			'zoomInRight'		=> esc_html__('Zoom In Right',		'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list of the out animations
if ( !function_exists( 'stevenwatkins_get_list_animations_out' ) ) {
	function stevenwatkins_get_list_animations_out($prepend_inherit=false) {
		$list = array(
			'none'			=> esc_html__('- None -',			'stevenwatkins'),
			'bounceOut'		=> esc_html__('Bounce Out',			'stevenwatkins'),
			'bounceOutUp'	=> esc_html__('Bounce Out Up',		'stevenwatkins'),
			'bounceOutDown'	=> esc_html__('Bounce Out Down',	'stevenwatkins'),
			'bounceOutLeft'	=> esc_html__('Bounce Out Left',	'stevenwatkins'),
			'bounceOutRight'=> esc_html__('Bounce Out Right',	'stevenwatkins'),
			'fadeOut'		=> esc_html__('Fade Out',			'stevenwatkins'),
			'fadeOutUp'		=> esc_html__('Fade Out Up',		'stevenwatkins'),
			'fadeOutUpBig'	=> esc_html__('Fade Out Up Big',	'stevenwatkins'),
			'fadeOutDownSmall'	=> esc_html__('Fade Out Down Small','stevenwatkins'),
			'fadeOutDownBig'=> esc_html__('Fade Out Down Big',	'stevenwatkins'),
			'fadeOutDown'	=> esc_html__('Fade Out Down',		'stevenwatkins'),
			'fadeOutLeft'	=> esc_html__('Fade Out Left',		'stevenwatkins'),
			'fadeOutLeftBig'=> esc_html__('Fade Out Left Big',	'stevenwatkins'),
			'fadeOutRight'	=> esc_html__('Fade Out Right',		'stevenwatkins'),
			'fadeOutRightBig'=> esc_html__('Fade Out Right Big','stevenwatkins'),
			'flipOutX'		=> esc_html__('Flip Out X',			'stevenwatkins'),
			'flipOutY'		=> esc_html__('Flip Out Y',			'stevenwatkins'),
			'hinge'			=> esc_html__('Hinge Out',			'stevenwatkins'),
			'lightSpeedOut'	=> esc_html__('Light Speed Out',	'stevenwatkins'),
			'rotateOut'		=> esc_html__('Rotate Out',			'stevenwatkins'),
			'rotateOutUpLeft'	=> esc_html__('Rotate Out Down Left',	'stevenwatkins'),
			'rotateOutUpRight'	=> esc_html__('Rotate Out Up Right',	'stevenwatkins'),
			'rotateOutDownLeft'	=> esc_html__('Rotate Out Up Left',		'stevenwatkins'),
			'rotateOutDownRight'=> esc_html__('Rotate Out Down Right',	'stevenwatkins'),
			'rollOut'			=> esc_html__('Roll Out',		'stevenwatkins'),
			'slideOutUp'		=> esc_html__('Slide Out Up',	'stevenwatkins'),
			'slideOutDown'		=> esc_html__('Slide Out Down',	'stevenwatkins'),
			'slideOutLeft'		=> esc_html__('Slide Out Left',	'stevenwatkins'),
			'slideOutRight'		=> esc_html__('Slide Out Right','stevenwatkins'),
			'zoomOut'			=> esc_html__('Zoom Out',		'stevenwatkins'),
			'zoomOutUp'			=> esc_html__('Zoom Out Up',	'stevenwatkins'),
			'zoomOutDown'		=> esc_html__('Zoom Out Down',	'stevenwatkins'),
			'zoomOutLeft'		=> esc_html__('Zoom Out Left',	'stevenwatkins'),
			'zoomOutRight'		=> esc_html__('Zoom Out Right',	'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return classes list for the specified animation
if (!function_exists('stevenwatkins_get_animation_classes')) {
	function stevenwatkins_get_animation_classes($animation, $speed='normal', $loop='none') {
		// speed:	fast=0.5s | normal=1s | slow=2s
		// loop:	none | infinite
		return stevenwatkins_is_off($animation) ? '' : 'animated '.esc_attr($animation).' '.esc_attr($speed).(!stevenwatkins_is_off($loop) ? ' '.esc_attr($loop) : '');
	}
}

// Return custom sidebars list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'stevenwatkins_get_list_sidebars' ) ) {
	function stevenwatkins_get_list_sidebars($prepend_inherit=false) {
		if (($list = stevenwatkins_storage_get('list_sidebars'))=='') {
			$list = apply_filters('stevenwatkins_filter_list_sidebars', array(
				'sidebar_widgets'		=> esc_html__('Sidebar Widgets', 'stevenwatkins'),
				'header_widgets'		=> esc_html__('Header Widgets', 'stevenwatkins'),
				'above_page_widgets'	=> esc_html__('Above Page Widgets', 'stevenwatkins'),
				'above_content_widgets' => esc_html__('Above Content Widgets', 'stevenwatkins'),
				'below_content_widgets' => esc_html__('Below Content Widgets', 'stevenwatkins'),
				'below_page_widgets' 	=> esc_html__('Below Page Widgets', 'stevenwatkins'),
				'footer_widgets'		=> esc_html__('Footer Widgets', 'stevenwatkins')
				)
			);
			$custom_sidebars_number = max(0, min(10, stevenwatkins_get_theme_setting('custom_sidebars')));
			if (count($custom_sidebars_number) > 0) {
				for ($i=1; $i <= $custom_sidebars_number; $i++) {
					$list['custom_widgets_'.intval($i)] = sprintf(esc_html__('Custom Widgets %d', 'stevenwatkins'), $i);
				}
			}
			stevenwatkins_storage_set('list_sidebars', $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return sidebars positions
if ( !function_exists( 'stevenwatkins_get_list_sidebars_positions' ) ) {
	function stevenwatkins_get_list_sidebars_positions($prepend_inherit=false) {
		$list = array(
			'left'  => esc_html__('Left',  'stevenwatkins'),
			'right' => esc_html__('Right', 'stevenwatkins')
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return blog styles list, prepended inherit
if ( !function_exists( 'stevenwatkins_get_list_blog_styles' ) ) {
	function stevenwatkins_get_list_blog_styles($prepend_inherit=false) {
		$list = apply_filters('stevenwatkins_filter_list_blog_styles', array(
			'excerpt'	=> esc_html__('Excerpt','stevenwatkins'),
			'classic_2'	=> esc_html__('Classic /2 columns/',	'stevenwatkins'),
			'classic_3'	=> esc_html__('Classic /3 columns/',	'stevenwatkins')
			)
		);
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list of categories
if ( !function_exists( 'stevenwatkins_get_list_categories' ) ) {
	function stevenwatkins_get_list_categories($prepend_inherit=false) {
		if (($list = stevenwatkins_storage_get('list_categories'))=='') {
			$list = array();
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false );
			$taxonomies = get_categories( $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;
				}
			}
			stevenwatkins_storage_set('list_categories', $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list of taxonomies
if ( !function_exists( 'stevenwatkins_get_list_terms' ) ) {
	function stevenwatkins_get_list_terms($prepend_inherit=false, $taxonomy='category') {
		if (($list = stevenwatkins_storage_get('list_taxonomies_'.($taxonomy)))=='') {
			$list = array();
			$args = array(
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => $taxonomy,
				'pad_counts'               => false );
			$taxonomies = get_terms( $taxonomy, $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;	// . ($taxonomy!='category' ? ' /'.($cat->taxonomy).'/' : '');
				}
			}
			stevenwatkins_storage_set('list_taxonomies_'.($taxonomy), $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return list of post's types
if ( !function_exists( 'stevenwatkins_get_list_posts_types' ) ) {
	function stevenwatkins_get_list_posts_types($prepend_inherit=false) {
		if (($list = stevenwatkins_storage_get('list_posts_types'))=='') {
			$list = apply_filters('stevenwatkins_filter_list_posts_types', array(
				'post' => esc_html('Post', 'stevenwatkins')
			));
			stevenwatkins_storage_set('list_posts_types', $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'stevenwatkins_get_list_posts' ) ) {
	function stevenwatkins_get_list_posts($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'taxonomy'			=> 'category',
			'taxonomy_value'	=> '',
			'posts_per_page'	=> -1,
			'orderby'			=> 'post_date',
			'order'				=> 'desc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));

		$hash = 'list_posts_'.($opt['post_type']).'_'.($opt['taxonomy']).'_'.($opt['taxonomy_value']).'_'.($opt['orderby']).'_'.($opt['order']).'_'.($opt['return']).'_'.($opt['posts_per_page']);
		if (($list = stevenwatkins_storage_get($hash))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'stevenwatkins');
			$args = array(
				'post_type' => $opt['post_type'],
				'post_status' => $opt['post_status'],
				'posts_per_page' => $opt['posts_per_page'],
				'ignore_sticky_posts' => true,
				'orderby'	=> $opt['orderby'],
				'order'		=> $opt['order']
			);
			if (!empty($opt['taxonomy_value'])) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $opt['taxonomy'],
						'field' => (int) $opt['taxonomy_value'] > 0 ? 'id' : 'slug',
						'terms' => $opt['taxonomy_value']
					)
				);
			}
			$posts = get_posts( $args );
			if (is_array($posts) && count($posts) > 0) {
				foreach ($posts as $post) {
					$list[$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
				}
			}
			stevenwatkins_storage_set($hash, $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}


// Return list of registered users
if ( !function_exists( 'stevenwatkins_get_list_users' ) ) {
	function stevenwatkins_get_list_users($prepend_inherit=false, $roles=array('administrator', 'editor', 'author', 'contributor', 'shop_manager')) {
		if (($list = stevenwatkins_storage_get('list_users'))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'stevenwatkins');
			$args = array(
				'orderby'	=> 'display_name',
				'order'		=> 'ASC' );
			$users = get_users( $args );
			if (is_array($users) && count($users) > 0) {
				foreach ($users as $user) {
					$accept = true;
					if (is_array($user->roles)) {
						if (is_array($user->roles) && count($user->roles) > 0) {
							$accept = false;
							foreach ($user->roles as $role) {
								if (in_array($role, $roles)) {
									$accept = true;
									break;
								}
							}
						}
					}
					if ($accept) $list[$user->user_login] = $user->display_name;
				}
			}
			stevenwatkins_storage_set('list_users', $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return menus list, prepended inherit
if ( !function_exists( 'stevenwatkins_get_list_menus' ) ) {
	function stevenwatkins_get_list_menus($prepend_inherit=false) {
		if (($list = stevenwatkins_storage_get('list_menus'))=='') {
			$list = array();
			$list['default'] = esc_html__("Default", 'stevenwatkins');
			$menus = wp_get_nav_menus();
			if (is_array($menus) && count($menus) > 0) {
				foreach ($menus as $menu) {
					$list[$menu->slug] = $menu->name;
				}
			}
			stevenwatkins_storage_set('list_menus', $list);
		}
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}

// Return iconed classes list
if ( !function_exists( 'stevenwatkins_get_list_icons' ) ) {
	function stevenwatkins_get_list_icons($prepend_inherit=false) {
		static $list = false;
		if (!is_array($list)) 
			$list = !is_admin() ? array() : stevenwatkins_parse_icons_classes(stevenwatkins_get_file_dir("css/fontello/css/fontello-codes.css"));
		return $prepend_inherit ? stevenwatkins_array_merge(array('inherit' => esc_html__("Inherit", 'stevenwatkins')), $list) : $list;
	}
}
?>