<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('stevenwatkins_storage_get')) {
	function stevenwatkins_storage_get($var_name, $default='') {
		global $STEVENWATKINS_STORAGE;
		return isset($STEVENWATKINS_STORAGE[$var_name]) ? $STEVENWATKINS_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('stevenwatkins_storage_set')) {
	function stevenwatkins_storage_set($var_name, $value) {
		global $STEVENWATKINS_STORAGE;
		$STEVENWATKINS_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('stevenwatkins_storage_empty')) {
	function stevenwatkins_storage_empty($var_name, $key='', $key2='') {
		global $STEVENWATKINS_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($STEVENWATKINS_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($STEVENWATKINS_STORAGE[$var_name][$key]);
		else
			return empty($STEVENWATKINS_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('stevenwatkins_storage_isset')) {
	function stevenwatkins_storage_isset($var_name, $key='', $key2='') {
		global $STEVENWATKINS_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($STEVENWATKINS_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($STEVENWATKINS_STORAGE[$var_name][$key]);
		else
			return isset($STEVENWATKINS_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('stevenwatkins_storage_inc')) {
	function stevenwatkins_storage_inc($var_name, $value=1) {
		global $STEVENWATKINS_STORAGE;
		if (empty($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = 0;
		$STEVENWATKINS_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('stevenwatkins_storage_concat')) {
	function stevenwatkins_storage_concat($var_name, $value) {
		global $STEVENWATKINS_STORAGE;
		if (empty($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = '';
		$STEVENWATKINS_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('stevenwatkins_storage_get_array')) {
	function stevenwatkins_storage_get_array($var_name, $key, $key2='', $default='') {
		global $STEVENWATKINS_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($STEVENWATKINS_STORAGE[$var_name][$key]) ? $STEVENWATKINS_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($STEVENWATKINS_STORAGE[$var_name][$key][$key2]) ? $STEVENWATKINS_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('stevenwatkins_storage_set_array')) {
	function stevenwatkins_storage_set_array($var_name, $key, $value) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if ($key==='')
			$STEVENWATKINS_STORAGE[$var_name][] = $value;
		else
			$STEVENWATKINS_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('stevenwatkins_storage_set_array2')) {
	function stevenwatkins_storage_set_array2($var_name, $key, $key2, $value) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if (!isset($STEVENWATKINS_STORAGE[$var_name][$key])) $STEVENWATKINS_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$STEVENWATKINS_STORAGE[$var_name][$key][] = $value;
		else
			$STEVENWATKINS_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Merge array elements
if (!function_exists('stevenwatkins_storage_merge_array')) {
	function stevenwatkins_storage_merge_array($var_name, $key, $value) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if ($key==='')
			$STEVENWATKINS_STORAGE[$var_name] = array_merge($STEVENWATKINS_STORAGE[$var_name], $value);
		else
			$STEVENWATKINS_STORAGE[$var_name][$key] = array_merge($STEVENWATKINS_STORAGE[$var_name][$key], $value);
	}
}

// Add array element after the key
if (!function_exists('stevenwatkins_storage_set_array_after')) {
	function stevenwatkins_storage_set_array_after($var_name, $after, $key, $value='') {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if (is_array($key))
			stevenwatkins_array_insert_after($STEVENWATKINS_STORAGE[$var_name], $after, $key);
		else
			stevenwatkins_array_insert_after($STEVENWATKINS_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('stevenwatkins_storage_set_array_before')) {
	function stevenwatkins_storage_set_array_before($var_name, $before, $key, $value='') {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if (is_array($key))
			stevenwatkins_array_insert_before($STEVENWATKINS_STORAGE[$var_name], $before, $key);
		else
			stevenwatkins_array_insert_before($STEVENWATKINS_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('stevenwatkins_storage_push_array')) {
	function stevenwatkins_storage_push_array($var_name, $key, $value) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($STEVENWATKINS_STORAGE[$var_name], $value);
		else {
			if (!isset($STEVENWATKINS_STORAGE[$var_name][$key])) $STEVENWATKINS_STORAGE[$var_name][$key] = array();
			array_push($STEVENWATKINS_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('stevenwatkins_storage_pop_array')) {
	function stevenwatkins_storage_pop_array($var_name, $key='', $defa='') {
		global $STEVENWATKINS_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($STEVENWATKINS_STORAGE[$var_name]) && is_array($STEVENWATKINS_STORAGE[$var_name]) && count($STEVENWATKINS_STORAGE[$var_name]) > 0) 
				$rez = array_pop($STEVENWATKINS_STORAGE[$var_name]);
		} else {
			if (isset($STEVENWATKINS_STORAGE[$var_name][$key]) && is_array($STEVENWATKINS_STORAGE[$var_name][$key]) && count($STEVENWATKINS_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($STEVENWATKINS_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('stevenwatkins_storage_inc_array')) {
	function stevenwatkins_storage_inc_array($var_name, $key, $value=1) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if (empty($STEVENWATKINS_STORAGE[$var_name][$key])) $STEVENWATKINS_STORAGE[$var_name][$key] = 0;
		$STEVENWATKINS_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('stevenwatkins_storage_concat_array')) {
	function stevenwatkins_storage_concat_array($var_name, $key, $value) {
		global $STEVENWATKINS_STORAGE;
		if (!isset($STEVENWATKINS_STORAGE[$var_name])) $STEVENWATKINS_STORAGE[$var_name] = array();
		if (empty($STEVENWATKINS_STORAGE[$var_name][$key])) $STEVENWATKINS_STORAGE[$var_name][$key] = '';
		$STEVENWATKINS_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('stevenwatkins_storage_call_obj_method')) {
	function stevenwatkins_storage_call_obj_method($var_name, $method, $param=null) {
		global $STEVENWATKINS_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($STEVENWATKINS_STORAGE[$var_name]) ? $STEVENWATKINS_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($STEVENWATKINS_STORAGE[$var_name]) ? $STEVENWATKINS_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('stevenwatkins_storage_get_obj_property')) {
	function stevenwatkins_storage_get_obj_property($var_name, $prop, $default='') {
		global $STEVENWATKINS_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($STEVENWATKINS_STORAGE[$var_name]->$prop) ? $STEVENWATKINS_STORAGE[$var_name]->$prop : $default;
	}
}
?>