<?php
/**
 * Generate custom CSS for theme hovers
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

// Theme init priorities:
// 3 - add/remove Theme Options elements
if (!function_exists('stevenwatkins_hovers_theme_setup3')) {
	add_action( 'after_setup_theme', 'stevenwatkins_hovers_theme_setup3', 3 );
	function stevenwatkins_hovers_theme_setup3() {
		// Add 'Menu hover' option
		stevenwatkins_storage_set_array_before('options', 'search_style', array(
			'menu_hover' => array(
				"title" => esc_html__('Menu hover', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select hover effect to decorate main menu', 'stevenwatkins') ),
				"std" => 'fade',
				"options" => array(
					'fade'			=> esc_html__('Fade',		'stevenwatkins'),
					'fade_box'		=> esc_html__('Fade Box',	'stevenwatkins'),
					'slide_box'		=> esc_html__('Slide Box',	'stevenwatkins'),
					'roll_down'		=> esc_html__('Roll Down',	'stevenwatkins'),
				),
				"type" => "select"
				),
			'menu_animation_in' => array( 
				"title" => esc_html__('Submenu show animation', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select animation to show submenu ', 'stevenwatkins') ),
				"std" => "fadeInUpSmall",
				"options" => stevenwatkins_get_list_animations_in(),
				"type" => "select"
				),
			'menu_animation_out' => array( 
				"title" => esc_html__('Submenu hide animation', 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select animation to hide submenu ', 'stevenwatkins') ),
				"std" => "fadeOutDownSmall",
				"options" => stevenwatkins_get_list_animations_out(),
				"type" => "select"
				)
			)
		);
		// Add 'Buttons hover' option
		stevenwatkins_storage_set_array_before('options', 'sidebar_widgets', array(
			'button_hover' => array(
				"title" => esc_html__("Button's hover", 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select hover effect to decorate all theme buttons', 'stevenwatkins') ),
				"std" => 'slide_left',
				"options" => array(
					'default'		=> esc_html__('Fade',				'stevenwatkins'),
					'slide_left'	=> esc_html__('3D Hover',	'stevenwatkins')
				),
				"type" => "select"
			),
			'image_hover' => array(
				"title" => esc_html__("Image's hover", 'stevenwatkins'),
				"desc" => wp_kses_data( __('Select hover effect to decorate all theme images', 'stevenwatkins') ),
				"std" => 'icon',
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'stevenwatkins')
				),
				"options" => array(
					'dots'	=> esc_html__('Dots',	'stevenwatkins'),
					'icon'	=> esc_html__('Icon',	'stevenwatkins')
				),
				"type" => "select"
			) )
		);
	}
}

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('stevenwatkins_hovers_theme_setup9')) {
	add_action( 'after_setup_theme', 'stevenwatkins_hovers_theme_setup9', 9 );
	function stevenwatkins_hovers_theme_setup9() {
		add_action( 'wp_enqueue_scripts',		'stevenwatkins_hovers_frontend_scripts', 1010 );
		add_filter( 'stevenwatkins_filter_localize_script','stevenwatkins_hovers_localize_script' );
		add_filter( 'stevenwatkins_filter_merge_scripts',	'stevenwatkins_hovers_merge_scripts' );
		add_filter( 'stevenwatkins_filter_merge_styles',	'stevenwatkins_hovers_merge_styles' );
		add_filter( 'stevenwatkins_filter_get_css', 		'stevenwatkins_hovers_get_css', 10, 3 );
	}
}
	
// Enqueue hover styles and scripts
if ( !function_exists( 'stevenwatkins_hovers_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'stevenwatkins_hovers_frontend_scripts', 1010 );
	function stevenwatkins_hovers_frontend_scripts() {
		if ( stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('includes/theme.hovers/jquery.slidemenu.js')) && in_array(stevenwatkins_get_theme_option('menu_hover'), array('slide_line', 'slide_box')) )
			stevenwatkins_enqueue_script( 'slidemenu', stevenwatkins_get_file_url('includes/theme.hovers/jquery.slidemenu.js'), array('jquery') );
		if ( stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('includes/theme.hovers/theme.hovers.js')) )
			stevenwatkins_enqueue_script( 'stevenwatkins-hovers', stevenwatkins_get_file_url('includes/theme.hovers/theme.hovers.js'), array('jquery') );
		if ( stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('includes/theme.hovers/theme.hovers.css')) )
			stevenwatkins_enqueue_style( 'stevenwatkins-hovers',  stevenwatkins_get_file_url('includes/theme.hovers/theme.hovers.css'), array(), null );
	}
}

// Merge hover effects into single js
if (!function_exists('stevenwatkins_hovers_merge_scripts')) {
	//Handler of the add_filter( 'stevenwatkins_filter_merge_scripts', 'stevenwatkins_hovers_merge_scripts' );
	function stevenwatkins_hovers_merge_scripts($list) {
		$list[] = 'includes/theme.hovers/jquery.slidemenu.js';
		$list[] = 'includes/theme.hovers/theme.hovers.js';
		return $list;
	}
}

// Merge hover effects into single css
if (!function_exists('stevenwatkins_hovers_merge_styles')) {
	//Handler of the add_filter( 'stevenwatkins_filter_merge_styles', 'stevenwatkins_hovers_merge_styles' );
	function stevenwatkins_hovers_merge_styles($list) {
		$list[] = 'includes/theme.hovers/theme.hovers.css';
		return $list;
	}
}

// Add hover effect's vars into localize array
if (!function_exists('stevenwatkins_hovers_localize_script')) {
	//Handler of the add_filter( 'stevenwatkins_filter_localize_script','stevenwatkins_hovers_localize_script' );
	function stevenwatkins_hovers_localize_script($arr) {
		$arr['menu_hover'] = stevenwatkins_get_theme_option('menu_hover');
		$arr['menu_hover_color'] = stevenwatkins_get_scheme_color('text_hover', stevenwatkins_get_theme_option( 'menu_scheme' ));
		$arr['button_hover'] = stevenwatkins_get_theme_option('button_hover');
		return $arr;
	}
}

// Add hover icons on the featured image
if ( !function_exists('stevenwatkins_hovers_add_icons') ) {
	function stevenwatkins_hovers_add_icons($hover, $args=array()) {

		// Additional parameters
		$args = array_merge(array(
			'image' => null
		), $args);
	
		// Hover style 'Icons and 'Zoom'
		if (in_array($hover, array('icons', 'zoom'))) {
			if ($args['image'])
				$large_image = $args['image'];
			else {
				$attachment = wp_get_attachment_image_src( get_post_thumbnail_id(), 'masonry-big' );
				if (!empty($attachment[0]))
					$large_image = $attachment[0];
			}
			?>
			<div class="icons">
				<a href="<?php the_permalink(); ?>" aria-hidden="true" class="icon-link<?php if (empty($large_image)) echo ' single_icon'; ?>"></a>
				<?php if (!empty($large_image)) { ?>
				<a href="<?php echo esc_url($large_image); ?>" aria-hidden="true" class="icon-search" title="<?php echo esc_attr(get_the_title()); ?>"></a>
				<?php } ?>
			</div>
			<?php
	
		// Hover style 'Shop'
		} else if ($hover == 'shop') {
			global $product;
			?>
			<div class="icons">
				<?php
				if ($product->is_purchasable() && $product->is_in_stock()) {
					echo apply_filters( 'woocommerce_loop_add_to_cart_link',
										'<a rel="nofollow" href="' . esc_url($product->add_to_cart_url()) . '" 
														aria-hidden="true" 
														data-quantity="1" 
														data-product_id="' . esc_attr( $product->id ) . '"
														data-product_sku="' . esc_attr( $product->get_sku() ) . '"
														class="shop_cart button add_to_cart_button'
																. ' product_type_' . $product->product_type
																. ($product->supports( 'ajax_add_to_cart' ) ? ' ajax_add_to_cart' : '')
																. '">в корзину</a>',
										$product );
				}
				?>
				<a href="<?php the_permalink(); ?>" aria-hidden="true" class="shop_link button">Посмотреть</a>
			</div>
			<?php

		// Hover style 'Icon'
		} else if ($hover == 'icon') {
			?><div class="icons"><a href="<?php the_permalink(); ?>" aria-hidden="true" class="icon-add-mark"></a></div><?php

		// Hover style 'Dots'
		} else if ($hover == 'dots') {
			?><a href="<?php the_permalink(); ?>" aria-hidden="true" class="icons"><span></span><span></span><span></span></a><?php

		// Hover style 'Fade', 'Slide', 'Pull', 'Border'
		} else if (in_array($hover, array('fade', 'pull', 'slide', 'border'))) {
			?>
			<div class="post_info">
				<div class="post_info_back">
					<h4 class="post_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<div class="post_descr">
						<?php
						stevenwatkins_show_post_meta(array(
									'categories' => false,
									'date' => true,
									'edit' => false,
									'seo' => false,
									'share' => false,
									'counters' => 'comments,views',
									'echo' => true
									));
						// Remove the condition below if you want display excerpt
						if (false) {
							?><div class="post_excerpt"><?php the_excerpt(); ?></div><?php
						}
						?>
					</div>
				</div>
			</div>
			<?php
		}
	}
}

// Add styles into CSS
if ( !function_exists( 'stevenwatkins_hovers_get_css' ) ) {
	//Handler of the add_filter( 'stevenwatkins_filter_get_css', 'stevenwatkins_hovers_get_css', 10, 3 );
	function stevenwatkins_hovers_get_css($css, $colors, $fonts) {
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS
CSS;
		}

		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

/* ================= MAIN MENU ITEM'S HOVERS ==================== */

/* fade box */
/*
.menu_hover_fade_box .menu_main_nav > li.current-menu-item > a,
.menu_hover_fade_box .menu_main_nav > li.current-menu-parent > a,
.menu_hover_fade_box .menu_main_nav > li.current-menu-ancestor > a,
*/
.menu_hover_fade_box .menu_main_nav > a:hover,
.menu_hover_fade_box .menu_main_nav > li > a:hover,
.menu_hover_fade_box .menu_main_nav > li.sfHover > a {
	color: {$colors['alter_link']};
	background-color: {$colors['alter_bg_color']};
}

/* slide_line */
.menu_hover_slide_line .menu_main_nav > li#blob {
	background-color: {$colors['text_link']};
}

/* slide_box */
.menu_hover_slide_box .menu_main_nav > li#blob {
	background-color: {$colors['alter_bg_color']};
}

/* zoom_line */
.menu_hover_zoom_line .menu_main_nav > li > a:before {
	background-color: {$colors['text_link']};
}

/* path_line */
.menu_hover_path_line .menu_main_nav > li:before,
.menu_hover_path_line .menu_main_nav > li:after,
.menu_hover_path_line .menu_main_nav > li > a:before,
.menu_hover_path_line .menu_main_nav > li > a:after {
	background-color: {$colors['text_link']};
}

/* roll_down */
.menu_hover_roll_down .menu_main_nav > li > a:before {
	background-color: {$colors['text_link']};
}

/* color_line */
.menu_hover_color_line .menu_main_nav > li > a:before {
	background-color: {$colors['text_dark']};
}
.menu_hover_color_line .menu_main_nav > li > a:after,
.menu_hover_color_line .menu_main_nav > li.menu-item-has-children > a:after {
	background-color: {$colors['text_link']};
}
.menu_hover_color_line .menu_main_nav > li.sfHover > a,
.menu_hover_color_line .menu_main_nav > li > a:hover,
.menu_hover_color_line .menu_main_nav > li > a:focus {
	color: {$colors['text_link']};
}


/* ================= BUTTON'S HOVERS ==================== */

/* Slide */



/* ================= IMAGE'S HOVERS ==================== */

/* Common styles */
.post_featured .mask {
	background-color: {$colors['text_dark_07']};
}

/* Dots */
.post_featured.hover_dots:hover .mask {
	background-color: {$colors['text_dark_07']};
}
.post_featured.hover_dots .icons span {
	background-color: {$colors['text_link']};
}
.post_featured.hover_dots .post_info {
	color: {$colors['inverse_text']};
}

/* Icon */
.post_featured.hover_icon .icons a {
	color: {$colors['inverse_dark']};
}
.post_featured.hover_icon a:hover {
	color: {$colors['text_hover']};
}

/* Icon and Icons */
.post_featured.hover_icons .icons a {
	background-color: {$colors['bg_color_07']};
	color: {$colors['text_dark']};
}
.post_featured.hover_icons a:hover {
	background-color: {$colors['bg_color']};
	color: {$colors['text_link']};
}

/* Fade */
.post_featured.hover_fade .post_info,
.post_featured.hover_fade .post_info a,
.post_featured.hover_fade .post_info .post_meta_item,
.post_featured.hover_fade .post_info .post_meta .post_meta_item:before,
.post_featured.hover_fade .post_info .post_meta .post_meta_item:hover:before {
	color: {$colors['inverse_text']};
}
.post_featured.hover_fade .post_info a:hover {
	color: {$colors['text_link']};
}

/* Slide */
.post_featured.hover_slide .post_info,
.post_featured.hover_slide .post_info a,
.post_featured.hover_slide .post_info .post_meta_item,
.post_featured.hover_slide .post_info .post_meta .post_meta_item:before,
.post_featured.hover_slide .post_info .post_meta .post_meta_item:hover:before {
	color: {$colors['inverse_text']};
}
.post_featured.hover_slide .post_info a:hover {
	color: {$colors['text_link']};
}
.post_featured.hover_slide .post_info .post_title:after {
	background-color: {$colors['inverse_text']};
}

/* Pull */
.post_featured.hover_pull .post_info,
.post_featured.hover_pull .post_info a {
	color: {$colors['inverse_text']};
}
.post_featured.hover_pull .post_info a:hover {
	color: {$colors['text_link']};
}
.post_featured.hover_pull .post_info .post_descr {
	background-color: {$colors['text_dark']};
}

/* Border */
.post_featured.hover_border .post_info,
.post_featured.hover_border .post_info a,
.post_featured.hover_border .post_info .post_meta_item,
.post_featured.hover_border .post_info .post_meta .post_meta_item:before,
.post_featured.hover_border .post_info .post_meta .post_meta_item:hover:before {
	color: {$colors['inverse_text']};
}
.post_featured.hover_border .post_info a:hover {
	color: {$colors['text_link']};
}
.post_featured.hover_border .post_info:before,
.post_featured.hover_border .post_info:after {
	border-color: {$colors['inverse_text']};
}

/* Shop */
.post_featured.hover_shop .icons a {
	color: {$colors['bg_color']};
	border-color: {$colors['text_link']} !important;
	background-color: transparent;
}
.post_featured.hover_shop .icons a:hover {
	color: {$colors['inverse_text']};
	border-color: {$colors['text_link']} !important;
	background-color: {$colors['text_link']};
}

CSS;
		}
		
		return $css;
	}
}
?>