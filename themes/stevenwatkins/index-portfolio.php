<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

stevenwatkins_storage_set('blog_archive', true);

// Load scripts for both 'Gallery' and 'Portfolio' layouts!
stevenwatkins_enqueue_script( 'classie', stevenwatkins_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
stevenwatkins_enqueue_script( 'imagesloaded', stevenwatkins_get_file_url('js/theme.gallery/imagesloaded.min.js'), array(), null, true );
stevenwatkins_enqueue_script( 'masonry', stevenwatkins_get_file_url('js/theme.gallery/masonry.min.js'), array(), null, true );
stevenwatkins_enqueue_script( 'stevenwatkins-gallery-script', stevenwatkins_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$stevenwatkins_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$stevenwatkins_sticky_out = is_array($stevenwatkins_stickies) && count($stevenwatkins_stickies) > 0 && get_query_var( 'paged' ) < 1;
	
	// Show filters
	$stevenwatkins_show_filters = stevenwatkins_get_theme_option('show_filters');
	$stevenwatkins_tabs = array();
	if (!stevenwatkins_is_off($stevenwatkins_show_filters)) {
		$stevenwatkins_cat = stevenwatkins_get_theme_option('parent_cat');
		$stevenwatkins_post_type = stevenwatkins_get_theme_option('post_type');
		$stevenwatkins_taxonomy = stevenwatkins_get_post_type_taxonomy($stevenwatkins_post_type);
		$stevenwatkins_args = array(
			'type'			=> $stevenwatkins_post_type,
			'child_of'		=> $stevenwatkins_cat,
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 0,
			'exclude'		=> '',
			'include'		=> '',
			'number'		=> '',
			'taxonomy'		=> $stevenwatkins_taxonomy,
			'pad_counts'	=> false
		);
		$stevenwatkins_portfolio_list = get_terms($stevenwatkins_args);
		if (is_array($stevenwatkins_portfolio_list) && count($stevenwatkins_portfolio_list) > 0) {
			$stevenwatkins_tabs[$stevenwatkins_cat] = esc_html__('All', 'stevenwatkins');
			foreach ($stevenwatkins_portfolio_list as $stevenwatkins_term) {
				if (isset($stevenwatkins_term->term_id)) $stevenwatkins_tabs[$stevenwatkins_term->term_id] = $stevenwatkins_term->name;
			}
		}
	}
	if (count($stevenwatkins_tabs) > 0) {
		$stevenwatkins_portfolio_filters_ajax = true;
		$stevenwatkins_portfolio_filters_active = $stevenwatkins_cat;
		$stevenwatkins_portfolio_filters_id = 'portfolio_filters';
		if (!is_customize_preview())
			wp_enqueue_script('jquery-ui-tabs', false, array('jquery', 'jquery-ui-core'), null, true);
		?>
		<div class="portfolio_filters stevenwatkins_tabs stevenwatkins_tabs_ajax">
			<ul class="portfolio_titles stevenwatkins_tabs_titles">
				<?php
				foreach ($stevenwatkins_tabs as $stevenwatkins_id=>$stevenwatkins_title) {
					?><li><a href="<?php echo esc_url(stevenwatkins_get_hash_link(sprintf('#%s_%s_content', $stevenwatkins_portfolio_filters_id, $stevenwatkins_id))); ?>" data-tab="<?php echo esc_attr($stevenwatkins_id); ?>"><?php echo esc_html($stevenwatkins_title); ?></a></li><?php
				}
				?>
			</ul>
			<?php
			$stevenwatkins_ppp = stevenwatkins_get_theme_option('posts_per_page');
			if (stevenwatkins_is_inherit($stevenwatkins_ppp)) $stevenwatkins_ppp = '';
			foreach ($stevenwatkins_tabs as $stevenwatkins_id=>$stevenwatkins_title) {
				$stevenwatkins_portfolio_need_content = $stevenwatkins_id==$stevenwatkins_portfolio_filters_active || !$stevenwatkins_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr(sprintf('%s_%s_content', $stevenwatkins_portfolio_filters_id, $stevenwatkins_id)); ?>"
					class="portfolio_content stevenwatkins_tabs_content"
					data-blog-template="<?php echo esc_attr(stevenwatkins_storage_get('blog_template')); ?>"
					data-blog-style="<?php echo esc_attr(stevenwatkins_get_theme_option('blog_style')); ?>"
					data-posts-per-page="<?php echo esc_attr($stevenwatkins_ppp); ?>"
					data-post-type="<?php echo esc_attr($stevenwatkins_post_type); ?>"
					data-taxonomy="<?php echo esc_attr($stevenwatkins_taxonomy); ?>"
					data-cat="<?php echo esc_attr($stevenwatkins_id); ?>"
					data-parent-cat="<?php echo esc_attr($stevenwatkins_cat); ?>"
					data-need-content="<?php echo (false===$stevenwatkins_portfolio_need_content ? 'true' : 'false'); ?>"
				>
					<?php
					if ($stevenwatkins_portfolio_need_content) 
						stevenwatkins_show_portfolio_posts(array(
							'cat' => $stevenwatkins_id,
							'parent_cat' => $stevenwatkins_cat,
							'taxonomy' => $stevenwatkins_taxonomy,
							'post_type' => $stevenwatkins_post_type,
							'page' => 1,
							'sticky' => $stevenwatkins_sticky_out
							)
						);
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		stevenwatkins_show_portfolio_posts(array(
			'cat' => $stevenwatkins_id,
			'parent_cat' => $stevenwatkins_cat,
			'taxonomy' => $stevenwatkins_taxonomy,
			'post_type' => $stevenwatkins_post_type,
			'page' => 1,
			'sticky' => $stevenwatkins_sticky_out
			)
		);
	}

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>