<?php
/**
 * The template for homepage posts with "Excerpt" style
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

stevenwatkins_storage_set('blog_archive', true);

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	?><div class="posts_container"><?php
	
	$stevenwatkins_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$stevenwatkins_sticky_out = is_array($stevenwatkins_stickies) && count($stevenwatkins_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($stevenwatkins_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	while ( have_posts() ) { the_post(); 
		if ($stevenwatkins_sticky_out && !is_sticky()) {
			$stevenwatkins_sticky_out = false;
			?></div><?php
		}
		get_template_part( 'content', $stevenwatkins_sticky_out && is_sticky() ? 'sticky' : 'excerpt' );
	}
	if ($stevenwatkins_sticky_out) {
		$stevenwatkins_sticky_out = false;
		?></div><?php
	}
	
	?></div><?php

	stevenwatkins_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>