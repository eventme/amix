<?php
/**
 * The Footer: widgets area, logo, footer menu and socials
 *
 * @package WordPress
 * @subpackage STEVENWATKINS
 * @since STEVENWATKINS 1.0
 */

						// Widgets area inside page content
						stevenwatkins_create_widgets_area('widgets_below_content');
						?>				
					</div><!-- </.content> -->

					<?php
					// Show main sidebar
					get_sidebar();

					// Widgets area below page content
					stevenwatkins_create_widgets_area('widgets_below_page');

					$stevenwatkins_body_style = stevenwatkins_get_theme_option('body_style');
					if ($stevenwatkins_body_style != 'fullscreen') {
						?></div><!-- </.content_wrap> --><?php
					}
					?>
			</div><!-- </.page_content_wrap> -->

			<?php
			$stevenwatkins_footer_scheme =  stevenwatkins_is_inherit(stevenwatkins_get_theme_option('footer_scheme')) ? stevenwatkins_get_theme_option('color_scheme') : stevenwatkins_get_theme_option('footer_scheme');
			?>
			
			<footer class="site_footer_wrap scheme_<?php echo esc_attr($stevenwatkins_footer_scheme); ?>">
				<?php
				// Footer sidebar
				$stevenwatkins_footer_name = stevenwatkins_get_theme_option('footer_widgets');
				$stevenwatkins_footer_present = !stevenwatkins_is_off($stevenwatkins_footer_name) && is_active_sidebar($stevenwatkins_footer_name);
				if ($stevenwatkins_footer_present) { 
					stevenwatkins_storage_set('current_sidebar', 'footer');
					$stevenwatkins_footer_wide = stevenwatkins_get_theme_option('footer_wide');
					ob_start();
					do_action( 'stevenwatkins_action_before_sidebar' );
					if ( !dynamic_sidebar($stevenwatkins_footer_name) ) {
						// Put here html if user no set widgets in sidebar
					}
					do_action( 'stevenwatkins_action_after_sidebar' );
					$stevenwatkins_out = ob_get_contents();
					ob_end_clean();
					$stevenwatkins_out = preg_replace("/<\\/aside>[\r\n\s]*<aside/", "</aside><aside", $stevenwatkins_out);
					$stevenwatkins_need_columns = true;	//or check: strpos($stevenwatkins_out, 'columns_wrap')===false;
					if ($stevenwatkins_need_columns) {
						$stevenwatkins_columns = max(0, (int) stevenwatkins_get_theme_option('footer_columns'));
						if ($stevenwatkins_columns == 0) $stevenwatkins_columns = min(6, max(1, substr_count($stevenwatkins_out, '<aside ')));
						if ($stevenwatkins_columns > 1)
							$stevenwatkins_out = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($stevenwatkins_columns).' widget ', $stevenwatkins_out);
						else
							$stevenwatkins_need_columns = false;
					}
					?>
					<div class="footer_wrap widget_area<?php echo !empty($stevenwatkins_footer_wide) ? ' footer_fullwidth' : ''; ?>">
						<div class="footer_wrap_inner widget_area_inner">
							<?php 
							if (!$stevenwatkins_footer_wide) { 
								?><div class="content_wrap"><?php
							}
							if ($stevenwatkins_need_columns) {
								?><div class="columns_wrap"><?php
							}
							stevenwatkins_show_layout($stevenwatkins_out);
							if ($stevenwatkins_need_columns) {
								?></div><!-- /.columns_wrap --><?php
							}
							if (!$stevenwatkins_footer_wide) {
								?></div><!-- /.content_wrap --><?php
							}
							?>
						</div><!-- /.footer_wrap_inner -->
					</div><!-- /.footer_wrap -->
				<?php
				}
	
				// Logo
				if (stevenwatkins_is_on(stevenwatkins_get_theme_option('logo_in_footer'))) {
					$stevenwatkins_logo_image = '';
					if (stevenwatkins_get_retina_multiplier(2) > 1)
						$stevenwatkins_logo_image = stevenwatkins_get_theme_option( 'logo_footer_retina' );
					if (empty($stevenwatkins_logo_image)) 
						$stevenwatkins_logo_image = stevenwatkins_get_theme_option( 'logo_footer' );
					$stevenwatkins_logo_text   = get_bloginfo( 'name' );
					if (!empty($stevenwatkins_logo_image) || !empty($stevenwatkins_logo_text)) {
						?>
						<div class="logo_footer_wrap">
							<div class="logo_footer_wrap_inner">
								<?php
								if (!empty($stevenwatkins_logo_image)) {
									$stevenwatkins_attr = stevenwatkins_getimagesize($stevenwatkins_logo_image);
									echo '<a href="'.esc_url(home_url('/')).'"><img src="'.esc_url($stevenwatkins_logo_image).'" class="logo_footer_image" alt=""'.(!empty($stevenwatkins_attr[3]) ? sprintf(' %s', $stevenwatkins_attr[3]) : '').'></a>' ;
								} else if (!empty($stevenwatkins_logo_text)) {
									echo '<h1 class="logo_footer_text"><a href="'.esc_url(home_url('/')).'">' . esc_html($stevenwatkins_logo_text) . '</a></h1>';
								}
								?>
							</div>
						</div>
						<?php
					}
				}

				// Socials
				if ( stevenwatkins_is_on(stevenwatkins_get_theme_option('socials_in_footer')) && ($stevenwatkins_output = stevenwatkins_get_socials_links()) != '') {
					?>
					<div class="socials_footer_wrap socials_wrap">
						<div class="socials_footer_wrap_inner">
							<?php stevenwatkins_show_layout($stevenwatkins_output); ?>
						</div>
					</div>
					<?php
				}
				
				// Footer menu
				$stevenwatkins_menu_footer = stevenwatkins_get_nav_menu('menu_footer');
				if (!empty($stevenwatkins_menu_footer)) {
					?>
					<div class="menu_footer_wrap">
						<div class="menu_footer_wrap_inner">
							<?php stevenwatkins_show_layout($stevenwatkins_menu_footer); ?>
						</div>
					</div>
					<?php
				}
				?> 
			</footer><!-- /.site_footer_wrap -->
			
		</div><!-- /.page_wrap -->

	</div><!-- /.body_wrap -->

	<?php if (stevenwatkins_is_on(stevenwatkins_get_theme_option('debug_mode')) && file_exists(stevenwatkins_get_file_dir('images/makeup.jpg'))) { ?>
		<img src="<?php echo esc_url(stevenwatkins_get_file_url('images/makeup.jpg')); ?>" id="makeup">
	<?php } ?>

	<?php wp_footer(); ?>

</body>
</html>